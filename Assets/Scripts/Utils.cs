 using UnityEngine;

 public static class Utils {
   public static Vector3 CalculateTargetCenterPosition (SpriteRenderer target, bool onTop = true, float verticalOffset = 0, float horizontalOffset = 0) {
     return target.bounds.center + new Vector3 (0 + horizontalOffset, (onTop ? target.bounds.extents.y : -target.bounds.extents.y) + verticalOffset, 0);
   }

   public static float CalculateDesiredXPos (float step, float amount, int index) {
     float totalDistance = step * amount;
     float startingPoint = -(totalDistance / 2) + (step / 2);

     return startingPoint + step * index;
   }

   public static int GetRandomNumber (int max) {
     return Random.Range (0, max);
   }

   public static Quaternion RotateTowardsTarget (Transform myTransform, Transform targetTransform) {
     Vector3 difference = targetTransform.position - myTransform.position;
     float rotationZ = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg;

     return Quaternion.Euler (0.0f, 0.0f, rotationZ);
   }

   public static Vector3 MoveTowardsTarget (Vector3 myPos, Vector3 targetPos, float speed) {
     return Vector3.MoveTowards (myPos, targetPos, Time.deltaTime * speed);
   }
 }