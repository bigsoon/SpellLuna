﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
    public static SceneLoader instance;

    public Animator animator;

    private int sceneToLoad;

    public string GetSceneName () => SceneManager.GetActiveScene ().name;

    public void ReloadScene () {
        int currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;

        SceneManager.LoadScene (currentSceneIndex);
    }

    public void OnAnimationEnd () {
        SceneManager.LoadScene (sceneToLoad);
    }

    public void LoadNextSceneNoAnimation () {
        int currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;

        sceneToLoad = currentSceneIndex + 1;

        SceneManager.LoadScene (sceneToLoad);
    }

    public void LoadNextScene () {
        int currentSceneIndex = SceneManager.GetActiveScene ().buildIndex;

        sceneToLoad = currentSceneIndex + 1;

        animator.SetTrigger ("end");
    }

    public void LoadSceneNoAnimation (int sceneIndex) {
        sceneToLoad = sceneIndex;

        SceneManager.LoadScene (sceneToLoad);
    }

    public void LoadScene (int sceneIndex) {
        sceneToLoad = sceneIndex;

        animator.SetTrigger ("end");
    }

    public void GoToNextRoom () {
        sceneToLoad = DungeonGenerator.instance.Rooms[DungeonBrain.instance.CurrentFloor];

        animator.SetTrigger ("end");
    }

    void Awake () {
        instance = this;
    }
}