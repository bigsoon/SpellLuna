using System.Collections;
using System.Collections.Generic;

public static class Constants {
  public const string SHOTTO = "Shotto";
  public const string PROTECTIUM = "Protectium";
  public const string ELECTRICUS = "Electricus";
  public const string EXPLODING_TWINS = "ExplodingTwins";

  public const int LIFE_FACE_MAX_PARTS = 4;

  public const int INITIAL_LIFE_FACES = 3;

  public const int HAND_SPELL_SLOTS = 5;

  public static Dictionary<string, int> MapSpellToCharges = new Dictionary<string, int> { { SHOTTO, 2 },
    { PROTECTIUM, 1 },
    { ELECTRICUS, 1 },
    { EXPLODING_TWINS, 1 },
  };

  public static Dictionary<string, int> MapSpellToIndex = new Dictionary<string, int> {
    {
    SHOTTO,
    0
    },
    {
    PROTECTIUM,
    1
    },
    {
    ELECTRICUS,
    2
    },
    {
    EXPLODING_TWINS,
    3
    },
  };

}