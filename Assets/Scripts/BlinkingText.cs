﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingText : MonoBehaviour {

    private Text text;

    IEnumerator Blink () {
        while (true) {
            switch (text.color.a.ToString ()) {
                case "0":
                    text.color = new Color (text.color.r, text.color.g, text.color.b, 0);
                    yield return new WaitForSeconds (0.1f);
                    text.color = new Color (text.color.r, text.color.g, text.color.b, 0.5f);
                    yield return new WaitForSeconds (0.1f);
                    text.color = new Color (text.color.r, text.color.g, text.color.b, 1);
                    yield return new WaitForSeconds (0.2f);
                    break;
                case "1":
                    text.color = new Color (text.color.r, text.color.g, text.color.b, 1);
                    yield return new WaitForSeconds (0.1f);
                    text.color = new Color (text.color.r, text.color.g, text.color.b, 0.5f);
                    yield return new WaitForSeconds (0.1f);
                    text.color = new Color (text.color.r, text.color.g, text.color.b, 0);
                    yield return new WaitForSeconds (0.2f);
                    break;
            }
        }
    }

    void StartBlinking () {
        StopCoroutine ("Blink");
        StartCoroutine ("Blink");
    }

    void StopBlinking () {
        StopCoroutine ("Blink");
    }

    void Awake () {
        text = GetComponent<Text> ();
    }

    void Start () {
        StartBlinking ();
    }
}