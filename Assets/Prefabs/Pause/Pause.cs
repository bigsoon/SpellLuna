﻿using UnityEngine;

public class Pause : MonoBehaviour {
    public static Pause instance;
    [SerializeField]
    private GameObject pauseButtonCanvas;
    [SerializeField]
    private GameObject pauseMenuCanvas;

    private void TogglePauseButton (bool flag) => pauseButtonCanvas.SetActive (flag);

    private void TogglePauseMenu (bool flag) => pauseMenuCanvas.SetActive (flag);

    public void Initialize () {
        TogglePauseButton (true);
    }

    public void PauseGame () {
        Time.timeScale = 0;

        TogglePauseMenu (true);
    }

    public void Resume () {
        Time.timeScale = 1;

        TogglePauseMenu (false);
    }

    public void TearDown () {
        TogglePauseButton (false);
        DungeonBrain.instance.SaveAndTearDown ();
        SceneLoader.instance.LoadSceneNoAnimation (1);
    }

    void Awake () {
        instance = this;
    }
}