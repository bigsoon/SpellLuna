﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeFace : MonoBehaviour {

    private GameObject[] skulls;

    public void ActivateSkulls (float lifeFaceParts) {
        if (lifeFaceParts > Constants.LIFE_FACE_MAX_PARTS) {
            lifeFaceParts = Constants.LIFE_FACE_MAX_PARTS;
        }

        lifeFaceParts--;

        for (int i = 0; i < skulls.Length; i++) {
            if (i == lifeFaceParts) {
                skulls[i].SetActive (true);
            } else {
                skulls[i].SetActive (false);
            }
        }
    }

    void Awake () {
        skulls = new GameObject[4];

        skulls[0] = transform.GetChild (0).gameObject;
        skulls[1] = transform.GetChild (1).gameObject;
        skulls[2] = transform.GetChild (2).gameObject;
        skulls[3] = transform.GetChild (3).gameObject;

        skulls[0].SetActive (false);
        skulls[1].SetActive (false);
        skulls[2].SetActive (false);
        skulls[3].SetActive (false);
    }

}