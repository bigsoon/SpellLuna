﻿using System.Collections.Generic;
using UnityEngine;

public class LifeSystem : MonoBehaviour {
    public static LifeSystem instance;
    [SerializeField]
    private LifeFace[] lifeFaces;

    private float _damageLimit;

    public float DamageLimit {
        get => _damageLimit;
        set => _damageLimit = value;
    }

    private int _currentDamage = 0;

    public int CurrentDamage {
        get => _currentDamage;
        set => _currentDamage = value;
    }

    private void UpdateLifeFaces () {
        for (int i = 0; i < lifeFaces.Length; i++) {
            float lifeFaceParts = _currentDamage - i * Constants.LIFE_FACE_MAX_PARTS;

            lifeFaces[i].ActivateSkulls (lifeFaceParts);
        }
    }

    public bool NotDamaged () => _currentDamage == 0;

    public bool Injured () => _currentDamage != 0;

    public void Heal (int amount) {
        bool shouldTurnOffWings = _currentDamage >= 8 && _currentDamage - amount < 8;
        bool shouldTurnOffTail = _currentDamage >= 4 && _currentDamage - amount < 4;

        if (_currentDamage - amount < 0) {
            _currentDamage = 0;
        } else {
            _currentDamage -= amount;
        }

        if (shouldTurnOffWings) {
            Luna.instance.ToggleWings (false);
        }

        if (shouldTurnOffTail) {
            Luna.instance.ToggleTail (false);
        }

        UpdateLifeFaces ();
    }

    public bool WillKillLuna (int damage) {
        return _currentDamage + damage >= _damageLimit;
    }

    public float GetTransformationCount () {
        return Mathf.Ceil (_currentDamage / Constants.LIFE_FACE_MAX_PARTS);
    }

    public void TakeDamage (int damage) {
        if (Luna.instance.HasActiveShields ()) {
            Instantiate (Luna.instance.ShieldEffectPrefab, Luna.instance.transform.position, Quaternion.identity);
            Luna.instance.RemoveShield ();
        } else {
            bool shouldTurnOnWings = _currentDamage < 8 && _currentDamage + damage >= 8;
            bool shouldTurnOnTail = _currentDamage < 4 && _currentDamage + damage >= 4;
            Instantiate (Luna.instance.BloodEffectPrefab, Luna.instance.transform.position, Quaternion.identity);
            _currentDamage += damage;

            if (shouldTurnOnTail) {
                Luna.instance.ToggleTail (true);
            }

            if (shouldTurnOnWings) {
                Luna.instance.ToggleWings (true);
            }
        }

        UpdateLifeFaces ();

        if (_currentDamage >= _damageLimit) {
            FightRoomHandler.instance.Lose ();
            return;
        }
    }

    void Awake () {
        instance = this;
    }

    void Start () {
        _damageLimit = Constants.INITIAL_LIFE_FACES * Constants.LIFE_FACE_MAX_PARTS;
        int damageToRemember = DungeonBrain.instance.DamageToRemember;

        if (damageToRemember > 0) {
            _currentDamage = damageToRemember;
        }
        UpdateLifeFaces ();
    }
}