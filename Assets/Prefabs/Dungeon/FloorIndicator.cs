using UnityEngine;
using UnityEngine.UI;

public class FloorIndicator : MonoBehaviour {
  [SerializeField]
  private Text floorCounter;
  void Start () {
    floorCounter.text = "Floor " + DungeonBrain.instance.CurrentFloor.ToString ();
  }
}