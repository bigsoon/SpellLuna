﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSystem : MonoBehaviour {
    public static LootSystem instance;
    [SerializeField]
    private Loots _loots;

    public Loots Loots {
        get => _loots;
    }

    private GameObject ChooseLootType (int monsterWorth) {
        int randomLootNumber = Random.Range (0, monsterWorth);

        if (monsterWorth <= 10) {
            return _loots.currency;
        }

        return _loots.currency;
    }

    public void SpawnArcadeGold (Vector3 targetPos) {
        Currency currency = Instantiate (_loots.currency, targetPos, Quaternion.identity).GetComponent<Currency> ();

        currency.ShouldMove = false;
        currency.MoveDown = true;
    }

    public void SpawnLoot (Vector3 targetPos, int monsterWorth) {
        bool isPlusSign = Utils.GetRandomNumber (2) == 1;
        float offsetValue = Utils.GetRandomNumber (20) / 10.0f;

        Vector3 randomOffset = new Vector3 (isPlusSign ? offsetValue : -offsetValue, isPlusSign ? -offsetValue : offsetValue);
        Instantiate (ChooseLootType (monsterWorth), targetPos + randomOffset, Quaternion.identity);
    }

    void Awake () {
        instance = this;
    }
}