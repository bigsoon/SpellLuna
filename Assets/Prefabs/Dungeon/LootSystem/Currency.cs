using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Currency : MonoBehaviour {
  [SerializeField]
  private Rigidbody2D rigidBody;
  public Rigidbody2D RigidBody {
    get => rigidBody;
  }
  private Vector3 _targetPos;
  private bool _shouldMove = true;
  public bool ShouldMove {
    get => _shouldMove;
    set => _shouldMove = value;
  }

  private bool _moveDown = false;
  public bool MoveDown {
    get => _moveDown;
    set => _moveDown = value;
  }

  void Awake () {
    _targetPos = new Vector3 (2.2f, 5.9f);
  }
  void Update () {
    if (_moveDown) {
      transform.position = Utils.MoveTowardsTarget (transform.position,
        transform.position + new Vector3 (0, -1000), ArcadeRoomHandler.instance.Speed);
      return;
    }

    if (!_shouldMove) {
      return;
    }

    bool reachedTarget = Vector3.Equals (transform.position, _targetPos);

    if (reachedTarget) {
      Gold.instance.Increase ();
      DungeonBrain.instance.IncreaseGold ();
      Destroy (gameObject);
    }

    transform.position = Utils.MoveTowardsTarget (transform.position, _targetPos, 7);
  }
}