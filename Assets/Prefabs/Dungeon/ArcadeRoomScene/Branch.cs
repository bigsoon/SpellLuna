﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Branch : MonoBehaviour {
    [SerializeField]
    private Rigidbody2D rigidBody;

    void FixedUpdate () {
        rigidBody.velocity = new Vector3 (0, -ArcadeRoomHandler.instance.Speed);
    }
}