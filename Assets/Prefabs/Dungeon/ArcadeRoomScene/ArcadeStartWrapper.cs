﻿using UnityEngine;

public class ArcadeStartWrapper : MonoBehaviour {
    public void OnCountdownEnd () {
        ArcadeRoomHandler.instance.StartArcade ();
    }
}