﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollableObject : MonoBehaviour {
    [SerializeField]
    private Rigidbody2D rigidBody;

    [SerializeField]
    private bool stopScrolling = false;

    void FixedUpdate () {
        if (stopScrolling) {
            rigidBody.velocity = Vector3.zero;
        } else {
            rigidBody.velocity = new Vector3 (0, -ArcadeRoomHandler.instance.Speed);
        }
    }

}