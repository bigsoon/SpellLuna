﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour {

    public BoxCollider2D backgroundCollider;

    private float backgroundHeight;

    private void RepeatBackground () {
        Vector3 backgroundOffset = new Vector3 (0, backgroundHeight * 2);

        transform.position = transform.position + backgroundOffset;
    }

    void Awake () {
        backgroundHeight = backgroundCollider.size.y;
    }

    void Update () {
        if (transform.position.y < -backgroundHeight) {
            RepeatBackground ();
        }
    }
}