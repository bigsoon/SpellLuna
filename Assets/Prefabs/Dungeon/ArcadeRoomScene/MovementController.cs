﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {
    public static MovementController instance;
    public enum MovementState {
        Left,
        Right,
    }

    [SerializeField]
    private Animator animator;

    private MovementState movementState = MovementState.Left;

    private bool movementDisabled = false;

    public void OnMoveEnd () {
        animator.enabled = false;
    }

    public void MoveLeft () {
        if (movementState == MovementState.Left || movementDisabled) {
            return;
        }
        transform.position = new Vector3 (-2, transform.position.y);
        movementState = MovementState.Left;
    }

    public void MoveRight () {
        if (movementState == MovementState.Right || movementDisabled) {
            return;
        }

        transform.position = new Vector3 (3, transform.position.y);
        movementState = MovementState.Right;
    }

    public void DisableMovement () {
        movementDisabled = true;
    }

    public void DisableAnimator () {
        animator.enabled = false;
    }

    public void MoveEnd () {
        switch (movementState) {
            case MovementState.Left:
                animator.Play ("MoveEndLeft");
                break;
            case MovementState.Right:
                animator.Play ("MoveEndRight");
                break;
        }
    }

    void Awake () {
        instance = this;
    }
}