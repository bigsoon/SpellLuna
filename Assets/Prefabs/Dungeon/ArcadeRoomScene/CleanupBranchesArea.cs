﻿using UnityEngine;

public class CleanupBranchesArea : MonoBehaviour {
    void OnCollisionEnter2D (Collision2D collision) {
        if (collision.gameObject.tag == "Branch") {
            ArcadeRoomHandler instance = ArcadeRoomHandler.instance;

            Destroy (collision.gameObject);
        }
    }
}