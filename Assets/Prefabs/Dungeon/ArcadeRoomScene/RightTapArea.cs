﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightTapArea : MonoBehaviour {

    void OnMouseDown () {
        MovementController.instance.MoveRight ();
    }

    void Update () {
        if (Input.GetKeyDown (KeyCode.RightArrow)) {
            MovementController.instance.MoveRight ();
        }
    }
}