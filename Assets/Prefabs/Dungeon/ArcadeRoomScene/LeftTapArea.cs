﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftTapArea : MonoBehaviour {
    void OnMouseDown () {
        MovementController.instance.MoveLeft ();
    }

    void Update () {
        if (Input.GetKeyDown (KeyCode.LeftArrow)) {
            MovementController.instance.MoveLeft ();
        }
    }
}