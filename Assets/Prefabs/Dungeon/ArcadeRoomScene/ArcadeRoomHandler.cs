﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcadeRoomHandler : MonoBehaviour {
    public static ArcadeRoomHandler instance;

    private enum ArcadeState {
        NotStarted,
        Started,
        End
    }

    [SerializeField]
    private Canvas ArcadeRoomUI;

    [SerializeField]
    private Text timerText;
    [SerializeField]
    private GameObject leftTapArea;
    [SerializeField]
    private GameObject rightTapArea;
    [SerializeField]
    private GameObject leftBranchPrefab;
    [SerializeField]
    private GameObject rightBranchPrefab;
    [SerializeField]
    private Fairy fairy;

    private ArcadeState arcadeState = ArcadeState.NotStarted;

    private float timer = 30;
    private int _speed = 10;
    public int Speed {
        get => _speed;
    }

    private int leftBranchCounter = 0;
    private int rightBranchCounter = 0;

    private const int MAX_BRANCHES = 3;

    private void ToggleUI (bool flag) {
        foreach (Transform child in ArcadeRoomUI.transform) {
            child.gameObject.SetActive (flag);
        }
    }

    private void Win () {
        Luna.instance.TalkFromAlways (new [] { "That was easy!", "Nice moves!", "I love you!" });
        StopSpeed ();
        CleanBranches ();

        Vector3 fairyPos = fairy.transform.position;

        Luna.instance.transform.SetParent (null);
        fairy.transform.SetParent (null);
        fairy.transform.position = fairyPos;

        MovementController.instance.DisableAnimator ();
        MovementController.instance.DisableMovement ();

        fairy.Deliver (new Vector3 (Luna.instance.transform.position.x, 8), () => {
            fairy.Idle ();
            DungeonBrain.instance.IncreaseFloor ();
            SceneLoader.instance.GoToNextRoom ();
        });
    }

    private void JumpDown () {
        DungeonBrain.instance.Fall ();
    }

    private void UpdateTimer () {
        timer -= Time.deltaTime;
        timerText.text = timer.ToString ("F2");

        if (timer <= 0.1) {
            timer = 0;
            timerText.text = "0";
            arcadeState = ArcadeState.End;

            Win ();
        }
    }

    private void SpawnGold (Vector3 targetPos) {
        int spawnChance = Utils.GetRandomNumber (11);

        if (spawnChance < 2) {
            LootSystem.instance.SpawnArcadeGold (targetPos + new Vector3 (0, 6));
        }

        if (spawnChance < 7) {
            LootSystem.instance.SpawnArcadeGold (targetPos + new Vector3 (0, 4));
        }

        LootSystem.instance.SpawnArcadeGold (targetPos + new Vector3 (0, 2));
    }

    private void SpawnBranch () {
        if (arcadeState == ArcadeState.End) {
            return;
        }

        int randomNumber = Utils.GetRandomNumber (2);
        GameObject branchPrefab;

        if (leftBranchCounter == MAX_BRANCHES && randomNumber == 0) {
            branchPrefab = rightBranchPrefab;
            rightBranchCounter++;
            leftBranchCounter = 0;

            Instantiate (branchPrefab, branchPrefab.transform.position, branchPrefab.transform.rotation);
            SpawnGold (branchPrefab.transform.position);
            Invoke ("SpawnBranch", 0.6f);
            return;
        }

        if (rightBranchCounter == MAX_BRANCHES && randomNumber == 1) {
            branchPrefab = leftBranchPrefab;
            leftBranchCounter++;
            rightBranchCounter = 0;

            Instantiate (branchPrefab, branchPrefab.transform.position, branchPrefab.transform.rotation);
            SpawnGold (branchPrefab.transform.position);
            Invoke ("SpawnBranch", 0.6f);
            return;
        }

        switch (randomNumber) {
            case 0:
                branchPrefab = leftBranchPrefab;
                leftBranchCounter++;
                rightBranchCounter = 0;
                break;
            case 1:
                branchPrefab = rightBranchPrefab;
                rightBranchCounter++;
                leftBranchCounter = 0;
                break;
            default:
                branchPrefab = leftBranchPrefab;
                break;
        }

        Instantiate (branchPrefab, branchPrefab.transform.position, branchPrefab.transform.rotation);
        SpawnGold (branchPrefab.transform.position);
        Invoke ("SpawnBranch", 0.6f);
    }

    private void CleanBranches () {
        GameObject[] branchesToCleanup = GameObject.FindGameObjectsWithTag ("Branch");

        foreach (GameObject branch in branchesToCleanup) {
            Destroy (branch);
        }
    }

    public void StopSpeed () {
        CancelInvoke ("IncreaseSpeed");
        _speed = 0;
    }

    public void IncreaseSpeed () {
        _speed += 1;
    }

    public void StartArcade () {
        Luna.instance.TalkFromAlways (new [] { "Obstacles...", "Prepare yourself!", "Arcades..." });
        arcadeState = ArcadeState.Started;
        leftTapArea.SetActive (true);
        rightTapArea.SetActive (true);

        ToggleUI (true);
        SpawnBranch ();
        InvokeRepeating ("IncreaseSpeed", 5, 5);
    }

    public void Lose () {
        Luna.instance.TalkFromAlways (new [] { "Uh, we go down...", "Goodbye sweet world...", "Could this fairy catch me?" });
        arcadeState = ArcadeState.End;
        StopSpeed ();

        MovementController.instance.DisableMovement ();
        MovementController.instance.MoveEnd ();

        fairy.Recall ();

        Invoke ("JumpDown", 3);
    }

    void Awake () {
        instance = this;
    }

    void Start () {
        timer = timer + DungeonBrain.instance.DangerLevel / 2;
    }

    void Update () {
        switch (arcadeState) {
            case ArcadeState.Started:
                UpdateTimer ();
                break;
            case ArcadeState.End:
                break;
        }
    }
}