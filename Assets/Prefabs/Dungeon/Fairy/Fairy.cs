﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fairy : MonoBehaviour {
    public enum State {
        Idle,
        Summon,

        Transform,
        Deliver,
        Recall
    }

    public FairyItems fairyItems;

    public Animator animator;

    public delegate void Callback ();
    [SerializeField]
    private float lunaOffsetY = 1.3f;

    private GameObject fairyItem;

    private Vector3 deliverPos;

    private State currentState = State.Idle;

    private Callback onSummonComplete;

    private Callback onTransformComplete;
    private Callback onDeliverComplete;

    public void Idle () {
        currentState = State.Idle;
    }

    public void Summon (Callback callback) {
        this.onSummonComplete = callback;
        currentState = State.Summon;
        animator.Play ("FairySummon");
    }

    public void HandleSummon () {
        onSummonComplete ();
        onSummonComplete = null;
    }

    public void Transform (Callback callback) {
        this.onTransformComplete = callback;
        currentState = State.Transform;
        animator.Play ("FairyTransform");
    }

    public void HandleTransform () {
        onTransformComplete ();
        onTransformComplete = null;
    }

    public void Deliver (Vector3 deliverPos, Callback callback) {
        this.deliverPos = deliverPos;
        this.onDeliverComplete = callback;
        currentState = State.Deliver;
    }

    public void HandleDeliver () {
        Vector3 offset = new Vector3 (0, lunaOffsetY);
        Vector3 targetPos = Luna.instance.transform.position + offset;

        bool reachedLuna = Vector3.Equals (transform.position, targetPos);

        if (reachedLuna) {
            Vector3 deliverPosFairy = Utils.MoveTowardsTarget (transform.position, deliverPos, 5);

            transform.position = deliverPosFairy;
            Luna.instance.transform.position = transform.position - offset;

            bool reachedDeliver = Vector3.Equals (deliverPosFairy, deliverPos);

            if (reachedDeliver) {
                onDeliverComplete ();
                onDeliverComplete = null;
            }

            return;
        }

        transform.position = Utils.MoveTowardsTarget (transform.position, targetPos, 5);
    }

    public void Recall () {
        currentState = State.Recall;
        animator.Play ("FairyRecall");
    }

    public void HandleRecall () {
        Destroy (this.gameObject);
    }

    void OnCollisionEnter2D (Collision2D collision) {
        if (collision.gameObject.tag == "Branch") {
            Recall ();
            ArcadeRoomHandler.instance.Lose ();
        } else if (collision.gameObject.tag == "Currency") {
            Currency currency = collision.gameObject.GetComponent<Currency> ();
            currency.ShouldMove = true;
            currency.MoveDown = false;
        }
    }

    void Update () {
        switch (currentState) {
            case State.Idle:
                break;
            case State.Summon:
                break;
            case State.Transform:
                break;
            case State.Deliver:
                HandleDeliver ();
                break;
            case State.Recall:
                break;
        }
    }
}