using UnityEngine;
using UnityEngine.EventSystems;

public class InfoTooltip : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
  // [SerializeField]
  // private Spell spell;
  [SerializeField]
  private GameObject tooltipPrefab;
  [SerializeField]
  private Animator _animator;
  public Animator Animator {
    get => _animator;
  }

  private bool isMouseDown = false;

  private float tooltipTimer;

  public void AdjustPosition (Vector3 offset) {
    tooltipPrefab.transform.localPosition -= offset;
  }

  public void OnPointerDown (PointerEventData eventData) {
    // if (!spell.CanUse) {
    //   return;
    // }

    isMouseDown = true;
  }

  public void OnPointerUp (PointerEventData eventData) {
    // if (!spell.CanUse) {
    //   return;
    // }

    // TODO move to SpellTooltip
    if (SpellSystem.instance != null) {
      if (SpellSystem.instance.IsDragging == true) {
        SpellSystem.instance.IsDragging = false;
        tooltipTimer = 0;
      }
    }

    isMouseDown = false;
  }

  private void HandleInfoTooltip () {
    if (TooltipManager.instance.ActiveInfoTooltip != null) {
      Animator activeTooltipSpellAnimator = TooltipManager.instance.ActiveInfoTooltip.GetComponent<InfoTooltip> ().Animator;

      activeTooltipSpellAnimator.Play ("HideInformationTooltip");

      if (TooltipManager.instance.ActiveInfoTooltip == gameObject) {
        TooltipManager.instance.ActiveInfoTooltip = null;
        return;
      }
    }

    _animator.Play ("ShowInformationTooltip");
    TooltipManager.instance.ActiveInfoTooltip = gameObject;
  }

  void Update () {
    if (isMouseDown) {
      tooltipTimer += Time.deltaTime;
    } else {
      if (tooltipTimer > 0.0f && tooltipTimer <= 0.2f) {
        HandleInfoTooltip ();
      }
      tooltipTimer = 0;
    }
  }
}