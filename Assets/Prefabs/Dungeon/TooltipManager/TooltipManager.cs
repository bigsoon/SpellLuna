﻿using UnityEngine;

public class TooltipManager : MonoBehaviour {
    public static TooltipManager instance;

    private GameObject _activeInfoTooltip;
    public GameObject ActiveInfoTooltip {
        get => _activeInfoTooltip;
        set => _activeInfoTooltip = value;
    }

    public void HideActiveTooltip () {
        if (_activeInfoTooltip != null) {
            Animator activeTooltipSpellAnimator = _activeInfoTooltip.GetComponent<InfoTooltip> ().Animator;
            activeTooltipSpellAnimator.Play ("HideInformationTooltip");
            _activeInfoTooltip = null;
        }
    }

    void Awake () {
        instance = this;
    }
}