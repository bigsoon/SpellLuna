﻿using System.Collections.Generic;
using UnityEngine;

public class TraitSystem : MonoBehaviour {
    public static TraitSystem instance;
    [SerializeField]
    private Traits _traits;

    public Traits Traits {
        get => _traits;
    }

    private List<GameObject> activeTraits;

    private void AnimateNewTrait (GameObject trait) {
        Animator animator = trait.GetComponent<Animator> ();
        animator.Play ("AddTrait");
    }

    public void AnimateTraitTrigger (GameObject trait) {
        Animator animator = trait.GetComponent<Animator> ();
        animator.Play ("TriggerTrait", -1, 0);
    }

    public void AddNewTrait (GameObject trait) {
        GameObject newTrait = Instantiate (trait, new Vector3 (-200, 590), Quaternion.identity);

        newTrait.transform.SetParent (transform, false);
        activeTraits.Add (newTrait);
        AnimateNewTrait (newTrait);
    }

    public void TryLifeStealTrait () {
        GameObject lifeStealTrait =
            activeTraits.Find ((t) => t.GetComponent<LifeSteal> () != null);

        if (lifeStealTrait == null) {
            return;
        }

        int randomChance = Utils.GetRandomNumber (10);

        if (randomChance < 2) {
            if (LifeSystem.instance.Injured ()) {
                LifeSystem.instance.Heal (1);
                AnimateTraitTrigger (lifeStealTrait.gameObject);
            }
        }
    }

    public void TryOverchargeTrait (ChargingSpell chargingSpell) {
        GameObject overchargeTrait =
            activeTraits.Find ((t) => t.GetComponent<Overcharge> () != null);

        if (overchargeTrait == null) {
            return;
        }

        int randomChance = Utils.GetRandomNumber (10);

        if (randomChance < 2) {
            chargingSpell.Recharge (1);
            AnimateTraitTrigger (overchargeTrait.gameObject);
        }
    }

    public void TryProtectorTrait () {
        GameObject protectorTrait = activeTraits.Find ((t) => t.GetComponent<Protector> () != null);

        if (protectorTrait == null) {
            return;
        }

        int randomChance = Utils.GetRandomNumber (10);

        Debug.Log (randomChance);

        if (randomChance < 2) {
            FightManager.instance.DamageAllMonsters (1);
            if (LifeSystem.instance.Injured ()) {
                LifeSystem.instance.Heal (1);
            }
            AnimateTraitTrigger (protectorTrait.gameObject);
        }
    }

    void Awake () {
        activeTraits = new List<GameObject> ();

        instance = this;
    }
}