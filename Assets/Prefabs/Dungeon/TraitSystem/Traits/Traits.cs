﻿using System;
using UnityEngine;

[Serializable]
public class Traits {
  public GameObject lifeSteal;
  public GameObject overcharge;
  public GameObject protector;
}