﻿using UnityEngine;

public class MonstersWavesRoomHandler : MonoBehaviour {
    public static MonstersWavesRoomHandler instance;
    [SerializeField]
    private Fairy fairy;

    void Start () {
        fairy.Deliver (new Vector3 (0, 8), () => {
            fairy.Idle ();
            DungeonBrain.instance.IncreaseFloor ();
            SceneLoader.instance.GoToNextRoom ();
        });
    }

    void Awake () {
        instance = this;
    }
}