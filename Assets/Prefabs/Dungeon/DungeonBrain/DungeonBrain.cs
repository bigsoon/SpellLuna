﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonBrain : MonoBehaviour {
    public static DungeonBrain instance;

    private int _currentFloor = 1;
    public int CurrentFloor {
        get => _currentFloor;
    }

    private int _damageToRemember = 0;

    public int DamageToRemember {
        get => _damageToRemember;
        set => _damageToRemember = value;
    }

    private int _dangerLevel = 1;

    private int _goldGathered = 0;

    public int GoldGathered {
        get => _goldGathered;
    }

    public int DangerLevel {
        get => _dangerLevel;
        set => _dangerLevel = value;
    }

    public void SaveAndTearDown () {
        PlayerData.instance.AddCurrency (_goldGathered);
        PlayerData.instance.SaveBestFloor (_currentFloor);

        _currentFloor = 1;
        _damageToRemember = 0;
        _dangerLevel = 1;
        _goldGathered = 0;
    }

    public void IncreaseGold () {
        _goldGathered++;
    }

    public void IncreaseFloor () {
        _currentFloor++;
        _dangerLevel++;
    }

    public void Fall () {
        _currentFloor--;

        DungeonGenerator.instance.GenerateDungeon ();
        DungeonGenerator.instance.FillFallWithMonsters (_currentFloor);
        SceneLoader.instance.GoToNextRoom ();
    }

    void OnApplicationQuit () {
        SaveAndTearDown ();
    }

    void Awake () {
        instance = this;
    }
}