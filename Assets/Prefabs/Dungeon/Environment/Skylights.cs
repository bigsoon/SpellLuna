﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skylights : MonoBehaviour {
    [SerializeField]
    private int spawnTimer = 2;
    IEnumerator ActivateSkyLight () {
        yield return new WaitForSeconds (1);
        while (true) {
            foreach (Transform child in transform) {
                child.gameObject.SetActive (true);
                yield return new WaitForSeconds (spawnTimer);
            }

            foreach (Transform child in transform) {
                child.gameObject.SetActive (false);
                yield return new WaitForSeconds (spawnTimer);
            }
        }
    }

    void Start () {
        StartCoroutine (ActivateSkyLight ());
    }
}