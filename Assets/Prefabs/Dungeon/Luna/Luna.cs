﻿using System.Collections.Generic;
using UnityEngine;

public class Luna : MonoBehaviour {
    public static Luna instance;
    [SerializeField]
    private GameObject shieldPrefab;
    [SerializeField]
    private GameObject _summonEffectPrefab;
    [SerializeField]
    private Animator animator;
    public Animator Animator {
        get => animator;
    }

    [SerializeField]
    private GameObject _tail;
    [SerializeField]
    private GameObject _wings;
    [SerializeField]
    private DialogSystem dialogSystem;
    [SerializeField]
    private GameObject _shieldEffectPrefab;
    public GameObject ShieldEffectPrefab {
        get => _shieldEffectPrefab;
    }

    [SerializeField]
    private GameObject _bloodEffectPrefab;
    public GameObject BloodEffectPrefab {
        get => _bloodEffectPrefab;
    }

    private List<Waypoint> waypoints;

    private Vector3 _basePos;
    public Vector3 BasePos {
        set => _basePos = value;
    }

    private bool isMoving = false;

    private List<GameObject> _activeShields;

    public List<GameObject> ActiveShields {
        get => _activeShields;
    }

    public void AdjustUIElement (GameObject objectToAdjust, float offset = 1.5f) {
        Vector3 activeIndicatorPos = Camera.main.WorldToScreenPoint (transform.position + new Vector3 (0, offset));
        objectToAdjust.transform.position = activeIndicatorPos;
    }

    private void Move () {
        Waypoint currentWaypoint = waypoints[0];
        float waypointSpeed = currentWaypoint.Speed;
        Vector3 waypointPos = currentWaypoint.Pos;
        bool reachedWaypoint = Vector3.Distance (transform.position, waypointPos) < 0.001f;

        if (reachedWaypoint) {
            waypoints.Remove (currentWaypoint);

            if (waypoints.Count == 0) {
                isMoving = false;
                return;
            }

            currentWaypoint = waypoints[0];
        }
        transform.position = Utils.MoveTowardsTarget (transform.position, waypointPos, waypointSpeed);
    }

    public void ToggleWings (bool flag) {
        Instantiate (_summonEffectPrefab, transform.position, transform.rotation);
        _wings.SetActive (flag);
    }

    public void ToggleTail (bool flag) {
        Instantiate (_summonEffectPrefab, transform.position, transform.rotation);
        _tail.SetActive (flag);
    }

    public void TalkAlways (string sentence) => dialogSystem.StartDialog (sentence);

    public void TalkSometimes (string sentence, int probability = 50) {
        int randomNumber = Utils.GetRandomNumber (100);

        if (randomNumber <= probability) {
            dialogSystem.StartDialog (sentence);
        }
    }

    public void TalkFromAlways (string[] sentences) {
        int randomSentenceIndex = Utils.GetRandomNumber (sentences.Length);

        dialogSystem.StartDialog (sentences[randomSentenceIndex]);
    }

    public void TalkFromSometimes (string[] sentences, int probability = 50) {
        int randomSentenceIndex = Utils.GetRandomNumber (sentences.Length);
        int randomNumber = Utils.GetRandomNumber (100);

        if (randomNumber <= probability) {
            dialogSystem.StartDialog (sentences[randomSentenceIndex]);
        }
    }

    public void RemoveShield () {
        GameObject shieldToRemove = _activeShields[_activeShields.Count - 1];

        _activeShields.Remove (shieldToRemove);
        Destroy (shieldToRemove);
    }

    public bool HasActiveShields () {
        return _activeShields.Count > 0;
    }

    public void ResetShields () {
        _activeShields.ForEach (activeShield => {
            Destroy (activeShield);
        });
        _activeShields.Clear ();
    }

    public void CreateShield (int shields) {
        for (int i = 0; i < shields; i++) {
            float offset = _activeShields.Count * 0.2f;
            GameObject shield = Instantiate (shieldPrefab, new Vector3 (-0.5f + offset, -1), Quaternion.identity);
            shield.transform.SetParent (transform);
            _activeShields.Add (shield);
        }
    }

    public void Hit (int damage) {
        LifeSystem.instance.TakeDamage (damage);

        waypoints.Add (new Waypoint (15, _basePos + new Vector3 (Random.Range (0, 2) == 0 ? -0.3f : 0.3f, 0)));
        waypoints.Add (new Waypoint (15, _basePos));

        isMoving = true;
    }

    void Awake () {
        waypoints = new List<Waypoint> ();
        _activeShields = new List<GameObject> ();

        instance = this;
    }

    void Update () {
        if (isMoving) {
            Move ();
        }
    }
}