  using UnityEngine;

  struct Waypoint {
    private float _speed;

    private Vector3 _pos;

    public float Speed {
      get => _speed;
    }

    public Vector3 Pos {
      get => _pos;
    }

    public Waypoint (float speed, Vector3 pos) {
      _speed = speed;
      _pos = pos;
    }
  }