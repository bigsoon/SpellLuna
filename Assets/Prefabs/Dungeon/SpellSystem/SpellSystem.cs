 using System.Collections.Generic;
 using System.Linq;
 using System;
 using UnityEngine.UI;
 using UnityEngine;

 public class SpellSystem : MonoBehaviour {
   public static SpellSystem instance;
   [SerializeField]
   private Text pendingSpellsCounter;
   [SerializeField]
   private Text usedSpellCounter;

   private List<Spell> spellBook;
   public List<Spell> SpellBook {
     get => spellBook;
   }

   private List<Spell> pendingSpells;
   private List<Spell> handSpells;
   private List<Spell> usedSpells;

   private GameObject draggingSpell;

   private bool _isDragging = false;

   public bool IsDragging {
     get => _isDragging;
     set => _isDragging = value;
   }

   private bool[] blockedSpellSlots = new bool[5];

   public bool[] BlockedSpellSlots {
     get => blockedSpellSlots;
   }

   public void BlockSpellSlot (int index) {
     blockedSpellSlots[index] = true;
   }

   public void ResetSpellSlots () {
     blockedSpellSlots[0] = false;
     blockedSpellSlots[1] = false;
     blockedSpellSlots[2] = false;
     blockedSpellSlots[3] = false;
     blockedSpellSlots[4] = false;
   }

   public GameObject GetDraggingSpell () => draggingSpell;

   public bool AllHandSpellsCharged () {
     bool areAllCharged = true;

     handSpells.ForEach ((handSpell) => {
       ChargingSpell chargingSpell = handSpell.GetComponent<ChargingSpell> ();
       if (chargingSpell != null && chargingSpell.NotFullyCharged ()) {
         areAllCharged = false;
       }
     });

     return areAllCharged;
   }

   public void SetDraggingSpell (GameObject spell) {
     draggingSpell = spell;
   }

   public Spell GetRandomHandSpell () {
     return handSpells[UnityEngine.Random.Range (0, handSpells.Count)];
   }

   public void IndicatePossibleChargingSpells () {
     handSpells.ForEach (s => {
       ChargingSpell spellToCharge = s.GetComponent<ChargingSpell> ();
       if (spellToCharge != null && spellToCharge.NotFullyCharged () && !spellToCharge.IsBlocked) {
         spellToCharge.PlayAnimation ("Shake");
       }
     });
   }

   public void ChargePossibleSpells () {
     handSpells.ForEach (s => {
       ChargingSpell spellToCharge = s.GetComponent<ChargingSpell> ();
       if (spellToCharge != null && spellToCharge.NotFullyCharged () && !spellToCharge.IsBlocked) {
         spellToCharge.Recharge (1);
         spellToCharge.PlayAnimation ("Refill");
       }
     });
   }

   public Spell CreateNewSpell (Spell spellPrefab) {
     Spell spellToAdd = Instantiate (spellPrefab, new Vector3 (0, 0), Quaternion.identity);
     spellToAdd.transform.SetParent (transform, false);

     return spellToAdd;
   }

   public int GetActiveChargesCountOnHand () {
     int counter = 0;

     foreach (Spell handSpell in handSpells) {
       ChargingSpell chargingSpell = handSpell.GetComponent<ChargingSpell> ();

       if (chargingSpell != null) {
         counter += chargingSpell.Charges;
       }
     }

     return counter;
   }

   public void DechargeHand () {
     foreach (Spell handSpell in handSpells) {
       ChargingSpell chargingSpell = handSpell.GetComponent<ChargingSpell> ();

       if (chargingSpell != null) {
         chargingSpell.Decharge ();
       }
     }
   }

   private void CreateSpellBook () {
     DeckGenerator.instance.Deck.ForEach (spell =>
       spellBook.Add (CreateNewSpell (spell)));
   }

   public void ShufflePendingSpells () {
     pendingSpells = pendingSpells.OrderBy (a => Guid.NewGuid ()).ToList ();
   }

   public List<Spell> GetPendingSpells () {
     return pendingSpells;
   }

   public void AddSpellToPending (Spell spell) {
     pendingSpells.Add (spell);
   }

   public void RemoveSpellFromPending (Spell spell) {
     pendingSpells.RemoveAt (pendingSpells.Count - 1);
   }

   public void CreatePendingSpells () {
     pendingSpells = new List<Spell> (spellBook);

     for (int i = 0; i < pendingSpells.Count; i++) {
       Spell spell = pendingSpells[i];
     }
   }

   private void FillPendingSpells () {
     for (int i = 0; i < usedSpells.Count; i++) {
       Spell usedSpell = usedSpells[i];

       usedSpell.MoveToPendingSpells ();
     }
   }

   public void DrawSpell (int handPosition) {
     Spell pendingSpell = pendingSpells[pendingSpells.Count - 1];

     if (blockedSpellSlots[handPosition]) {
       pendingSpell.IsBlocked = true;
     }

     pendingSpell.SetHandPosition (handPosition);
     pendingSpell.MoveToHand ();
   }

   public void FillHand () {
     for (int i = handSpells.Count; i < Constants.HAND_SPELL_SLOTS; i++) {
       if (pendingSpells.Count == 0) {
         FillPendingSpells ();
         break;
       }

       DrawSpell (i);
     }
   }

   public void CleanHand () {
     // Clean spells (AutoDestruct / Temporary etc.)
     Spell handSpell1 = null;
     Spell handSpell2 = null;
     Spell handSpell3 = null;
     Spell handSpell4 = null;
     Spell handSpell5 = null;

     if (handSpells.Count > 0) {
       handSpell1 = handSpells[0];
     }
     if (handSpells.Count > 1) {
       handSpell2 = handSpells[1];
     }
     if (handSpells.Count > 2) {
       handSpell3 = handSpells[2];
     }
     if (handSpells.Count > 3) {
       handSpell4 = handSpells[3];
     }
     if (handSpells.Count > 4) {
       handSpell5 = handSpells[4];
     }

     if (handSpell1 != null) {
       handSpell1.AutoDestruct (true);
     }
     if (handSpell2 != null) {
       handSpell2.AutoDestruct (true);
     }
     if (handSpell3 != null) {
       handSpell3.AutoDestruct (true);
     }
     if (handSpell4 != null) {
       handSpell4.AutoDestruct (true);
     }
     if (handSpell5 != null) {
       handSpell5.AutoDestruct (true);
     }

     for (int i = 0; i < handSpells.Count; i++) {
       ChargingSpell chargingSpell = handSpells[i].GetComponent<ChargingSpell> ();

       // TODO make longer fights harder?
       //  if (chargingSpell != null) {
       //    chargingSpell.FullyRecharge ();
       //  }

       handSpells[i].MoveToUsedSpells ();
     }
   }

   public List<Spell> GetHandSpells () {
     return handSpells;
   }

   public bool HasNoSpellsOnHand () {
     return handSpells.Count == 0;
   }

   public bool HasNoUsedSpells () {
     return usedSpells.Count == 0;
   }

   public void RemoveSpellFromUsed (Spell spell) {
     usedSpells.RemoveAt (usedSpells.Count - 1);
   }

   public void AddSpellToHand (Spell spell) {
     handSpells.Add (spell);
   }

   public void RemoveSpellFromHand (Spell spell) {
     handSpells.Remove (spell);
   }

   public List<Spell> GetUsedSpells () {
     return usedSpells;
   }

   public void AddSpellToUsed (Spell spell) {
     usedSpells.Add (spell);
   }

   void Awake () {
     spellBook = new List<Spell> ();
     pendingSpells = new List<Spell> ();
     handSpells = new List<Spell> ();
     usedSpells = new List<Spell> ();

     ResetSpellSlots ();
     CreateSpellBook ();

     instance = this;
   }

   void Update () {
     pendingSpellsCounter.text = pendingSpells.Count.ToString ();
     usedSpellCounter.text = usedSpells.Count.ToString ();
   }
 }