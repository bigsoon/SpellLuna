﻿using System.Collections.Generic;
using UnityEngine;

public class Charsho : Spell, ISpell {
    [SerializeField]
    private GameObject charshoEffectPrefab;

    public override void Cast (GameObject target) {
        List<Monster> monsters = FightManager.instance.Monsters;
        int activeChargesCount = SpellSystem.instance.GetActiveChargesCountOnHand ();

        base.Cast (target);
        FightManager.instance.DamageAllMonsters (1 + activeChargesCount);
        monsters.ForEach (m => Instantiate (charshoEffectPrefab, m.transform.position, Quaternion.identity));

        MoveToUsedSpells ();
    }
}