using UnityEngine;
using UnityEngine.EventSystems;

public class OnDragSpell : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {
  [SerializeField]
  private Spell spell;
  [SerializeField]
  private RectTransform rectTransform;
  [SerializeField]
  private RectTransform underneathRectTransform;
  [SerializeField]
  private CanvasGroup canvasGroup;
  private Canvas _fightRoomUICanvas;

  private void EnableSpellsDropping () {
    SpellSystem.instance.GetHandSpells ().ForEach (s => {
      if (s.GetComponent<Charagami> () != null) {
        return;
      }

      s.GetUnderneathWrapper ().GetComponent<CanvasGroup> ().blocksRaycasts = true;
    });
  }

  private void DisableSpellsDropping () {
    SpellSystem.instance.GetHandSpells ().ForEach (s => {
      s.GetUnderneathWrapper ().GetComponent<CanvasGroup> ().blocksRaycasts = false;
    });
  }

  public void OnBeginDrag (PointerEventData eventData) {
    if (spell.IsDisabled) {
      Luna.instance.TalkAlways ("Charge it first!");
    }
    if (spell.IsBlocked) {
      Luna.instance.TalkAlways ("Oh no! Monsters blocked this spell...");
    }
    if (!spell.CanDrag ()) {
      return;
    }

    SpellSystem.instance.IsDragging = true;
    TooltipManager.instance.HideActiveTooltip ();

    EnableSpellsDropping ();

    SpellSystem.instance.SetDraggingSpell (transform.parent.gameObject);
    canvasGroup.alpha = 1;
    canvasGroup.blocksRaycasts = false;
  }

  public void OnDrag (PointerEventData eventData) {
    if (!spell.CanDrag ()) {
      return;
    }

    rectTransform.anchoredPosition += eventData.delta / _fightRoomUICanvas.scaleFactor;
  }

  public void OnEndDrag (PointerEventData eventData) {
    DisableSpellsDropping ();

    SpellSystem.instance.SetDraggingSpell (null);
    canvasGroup.alpha = 0;
    canvasGroup.blocksRaycasts = true;

    rectTransform.anchoredPosition = underneathRectTransform.anchoredPosition;
  }

  void Awake () {
    _fightRoomUICanvas = FightRoomUI.instance.GetComponent<Canvas> ();
  }
}