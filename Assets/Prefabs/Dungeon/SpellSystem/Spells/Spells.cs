using System;

[Serializable]
public class Spells {
  public Shotto shotto;
  public Shotia shotia;
  public Protectium protectium;
  public Charagami charagami;
  public Electricus electricus;
  public Transformato transformato;
  public Blessy blessy;
  public Chargotto chargotto;
  public Charsho charsho;
  public Desintegrato desintegrato;
}