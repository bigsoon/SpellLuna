using UnityEngine;
using UnityEngine.UI;

public class TransformSpell : Spell, ISpell {
  [SerializeField]
  private GameObject autoDestructEffect;
  [SerializeField]
  private int _autoDestructCost = 2;
  public int AutoDestructCost {
    get => _autoDestructCost;
  }

  private void OnDestroy () {
    Destroy (gameObject);
  }

  public override void AutoDestruct (bool endTurn) {
    CanUse = false;
    SpellSystem.instance.RemoveSpellFromHand (this);
    Instantiate (autoDestructEffect, transform.position, Quaternion.identity);

    if (endTurn) {
      Luna.instance.Hit (_autoDestructCost / 2);
    } else {
      Luna.instance.Hit (_autoDestructCost);
    }

    Invoke ("OnDestroy", 0.3f);
  }

  public virtual void Cast (GameObject target) {
    base.Cast (target);
  }
}