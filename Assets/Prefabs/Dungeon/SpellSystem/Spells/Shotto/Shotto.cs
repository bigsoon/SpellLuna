﻿using UnityEngine;

public class Shotto : ChargingSpell, ISpell {
    [SerializeField]
    private GameObject shottoEffectPrefab;

    public override void Cast (GameObject target) {
        base.Cast (target);
        DecrementCharges ();

        target.GetComponent<Monster> ().TakeDamage (2);
        Instantiate (shottoEffectPrefab, target.transform.position, Quaternion.identity);
    }
}