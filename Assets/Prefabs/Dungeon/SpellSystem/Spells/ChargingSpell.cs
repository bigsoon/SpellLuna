using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChargingSpell : Spell, ISpell, IPointerEnterHandler, IPointerExitHandler {
  [SerializeField]
  private int charges;
  public int Charges {
    get => charges;
    set => charges = value;
  }

  [SerializeField]
  private Text chargesCounter;
  [SerializeField]
  private Text chargesTooltipCounter;
  private int initialCharges;

  public bool IsOutOfCharges () {
    return charges == 0;
  }

  public bool NotFullyCharged () {
    return charges < initialCharges;
  }

  private void CheckOutOfCharges () {
    if (IsOutOfCharges ()) {
      PlayAnimation ("OutOfCharges");
      IsDisabled = true;
    }
  }

  private void SetChargesText () {
    chargesCounter.text = charges.ToString ();
    chargesTooltipCounter.text = charges.ToString () + ".";
  }

  public bool FullyCharged () {
    return charges == initialCharges;
  }

  public void DecrementCharges () {
    charges--;

    PlayAnimation ("DecrementCharge");
    SetChargesText ();

    CheckOutOfCharges ();
  }

  public void FullyRecharge () {
    charges = initialCharges;
  }

  public void Recharge (int charges) {
    this.charges += charges;

    IsDisabled = false;
    SetChargesText ();
  }

  public void Decharge () {
    charges = 0;

    PlayAnimation ("DecrementCharge");
    SetChargesText ();

    CheckOutOfCharges ();
  }

  public void OnHand () {
    base.OnHand ();

    if (IsOutOfCharges ()) {
      IsDisabled = true;
    }
  }

  public virtual void Cast (GameObject target) {
    base.Cast (target);
  }

  public void OnPointerEnter (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();

    if (spell != null) {
      Charagami charagami = spell.GetComponent<Charagami> ();

      if (charagami != null) {
        if (NotFullyCharged () && !IsBlocked) {
          PlayAnimation ("Shake");
        }
      }
    }
  }

  public void OnPointerExit (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();

    if (spell != null) { }
  }

  void Start () {
    SetChargesText ();
  }

  void Awake () {
    initialCharges = charges;
  }
}