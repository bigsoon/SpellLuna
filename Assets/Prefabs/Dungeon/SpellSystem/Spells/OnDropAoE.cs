using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnDropAoE : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {
  private List<Monster> monsters;
  private Color baseColor;

  private void LightUpMonsters () {
    if (monsters == null) {
      return;
    }

    monsters.ForEach (m => {
      m.GetComponent<SpriteRenderer> ().material.color = Color.yellow;
      m.IsHovered = true;
    });
  }

  private void LightDownMonsters () {
    if (monsters == null) {
      return;
    }

    monsters.ForEach (m => {
      m.GetComponent<SpriteRenderer> ().material.color = baseColor;
      m.IsHovered = false;
    });
  }

  public void OnPointerEnter (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();

    if (spell != null) {
      Electricus electricus = spell.GetComponent<Electricus> ();
      Shotia shotia = spell.GetComponent<Shotia> ();
      Chargotto chargotto = spell.GetComponent<Chargotto> ();
      Charsho charsho = spell.GetComponent<Charsho> ();

      if (electricus != null || shotia != null || charsho != null) {
        LightUpMonsters ();
        return;
      }

      if (chargotto != null) {
        SpellSystem.instance.IndicatePossibleChargingSpells ();
      }
    }
  }
  public void OnPointerExit (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();

    if (spell != null) {
      Electricus electricus = spell.GetComponent<Electricus> ();
      Shotia shotia = spell.GetComponent<Shotia> ();
      Charsho charsho = spell.GetComponent<Charsho> ();

      if (electricus != null || shotia != null || charsho != null) {
        LightDownMonsters ();
        return;
      }
    }
  }

  public void OnDrop (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();
    Electricus electricus = spell.GetComponent<Electricus> ();
    Shotia shotia = spell.GetComponent<Shotia> ();
    Chargotto chargotto = spell.GetComponent<Chargotto> ();
    Charsho charsho = spell.GetComponent<Charsho> ();

    if (spell != null) {
      if (electricus != null) {
        electricus.Cast (gameObject);
        LightDownMonsters ();
        return;
      }
      if (shotia != null) {
        shotia.Cast (gameObject);
        LightDownMonsters ();
        return;
      }
      if (charsho != null) {
        charsho.Cast (gameObject);
        LightDownMonsters ();
        return;
      }
      if (chargotto != null) {
        chargotto.Cast (gameObject);
      }
    }
  }

  void Update () {
    if (monsters == null && FightManager.instance.HasAliveMonsters ()) {
      monsters = FightManager.instance.Monsters;
      baseColor = monsters[0].GetComponent<SpriteRenderer> ().material.color;
    }
  }
}