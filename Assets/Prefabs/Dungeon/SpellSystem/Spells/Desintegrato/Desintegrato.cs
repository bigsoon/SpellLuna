﻿using UnityEngine;

public class Desintegrato : Spell, ISpell {
    [SerializeField]
    private GameObject desintegratoEffectPrefab;

    public override void Cast (GameObject target) {
        int chargesDamage = SpellSystem.instance.GetActiveChargesCountOnHand () * 2;

        base.Cast (target);

        target.GetComponent<Monster> ().TakeDamage (2 + chargesDamage);
        SpellSystem.instance.DechargeHand ();
        Instantiate (desintegratoEffectPrefab, target.transform.position, Quaternion.identity);

        MoveToUsedSpells ();
    }
}