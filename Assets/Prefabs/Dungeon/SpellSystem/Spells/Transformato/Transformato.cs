﻿using UnityEngine;

public class Transformato : TransformSpell, ISpell {
    [SerializeField]
    private GameObject transformatoEffectPrefab;

    public override void Cast (GameObject target) {
        if (LifeSystem.instance.WillKillLuna (AutoDestructCost) && !Luna.instance.HasActiveShields ()) {
            Luna.instance.TalkFromAlways (new [] { "Do you really want to kill me just now...", "I will transform into a monster...", "No! Don't do that now!" });
            return;
        }

        base.Cast (target);

        target.GetComponent<Monster> ().TakeDamage (5);
        Instantiate (transformatoEffectPrefab, target.transform.position, Quaternion.identity);

        AutoDestruct (false);
    }
}