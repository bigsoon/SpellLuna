﻿using UnityEngine;

public class Chargotto : Spell, ISpell {

    public override void Cast (GameObject target) {
        if (SpellSystem.instance.AllHandSpellsCharged ()) {
            Luna.instance.TalkAlways ("Honey, all spells are charged already, it's fine...");
            return;
        }

        base.Cast (target);

        SpellSystem.instance.ChargePossibleSpells ();

        MoveToUsedSpells ();
    }
}