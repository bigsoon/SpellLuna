﻿using UnityEngine;

public class Protectium : ChargingSpell, ISpell {
    [SerializeField]
    private GameObject protectiumEffectPrefab;
    public override void Cast (GameObject target) {
        TraitSystem.instance.TryProtectorTrait ();
        base.Cast (target);
        DecrementCharges ();

        Instantiate (protectiumEffectPrefab, target.transform.position, Quaternion.identity);
        Luna.instance.CreateShield (1);
    }
}