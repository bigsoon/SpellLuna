﻿using System.Collections.Generic;
using UnityEngine;

public class Electricus : Spell, ISpell {
    [SerializeField]
    private GameObject eletricusEffectPrefab;

    public override void Cast (GameObject target) {
        List<Monster> monsters = FightManager.instance.Monsters;
        float transformationCount = LifeSystem.instance.GetTransformationCount ();

        base.Cast (target);
        FightManager.instance.DamageAllMonsters (3);
        monsters.ForEach (m => Instantiate (eletricusEffectPrefab, m.transform.position, Quaternion.identity));

        switch (transformationCount) {
            case 0:
                break;
            case 1:
            case 2:
                Monster randomMonster = FightManager.instance.GetRandomMonster ();
                randomMonster.Stun ();
                break;
        }

        MoveToUsedSpells ();
    }
}