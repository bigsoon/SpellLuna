﻿using UnityEngine;

public class Blessy : ChargingSpell, ISpell {
    [SerializeField]
    private GameObject blessyEffectPrefab;
    public override void Cast (GameObject target) {
        if (LifeSystem.instance.NotDamaged ()) {
            Luna.instance.TalkFromAlways (new [] { "Dear, you can't heal me when I am full!", "Leave Blessy when we will need it..." });
            return;
        }

        base.Cast (target);
        DecrementCharges ();

        Instantiate (blessyEffectPrefab, target.transform.position, Quaternion.identity);
        LifeSystem.instance.Heal (1);
    }
}