﻿using System.Collections.Generic;
using UnityEngine;

public class Shotia : ChargingSpell, ISpell {
    [SerializeField]
    private GameObject shotiaEffectPrefab;

    public override void Cast (GameObject target) {
        List<Monster> monsters = FightManager.instance.Monsters;

        for (int i = 0; i < 3; i++) {
            Monster randomMonster = FightManager.instance.GetRandomMonster ();

            if (randomMonster == null) {
                return;
            }

            randomMonster.TakeDamage (1);
            Instantiate (shotiaEffectPrefab, randomMonster.transform.position, Quaternion.identity);
        }

        base.Cast (target);
        DecrementCharges ();
    }
}