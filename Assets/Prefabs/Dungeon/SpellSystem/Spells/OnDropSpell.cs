using UnityEngine;
using UnityEngine.EventSystems;

public class OnDropSpell : MonoBehaviour, IDropHandler {
  [SerializeField]
  private ChargingSpell chargingSpell;

  public void OnDrop (PointerEventData eventData) {
    Charagami charagami = eventData.pointerDrag.transform.parent.GetComponent<Charagami> ();
    if (chargingSpell.FullyCharged () && charagami != null) {
      Luna.instance.TalkAlways ("Sweetie, this spell is charged already...");
      return;
    } else if (chargingSpell.FullyCharged () && charagami == null) {
      Luna.instance.TalkAlways ("I don't think this spell can be used for charging...");
      return;
    }

    if (chargingSpell.IsBlocked) {
      Luna.instance.TalkAlways ("Oh no! Monsters blocked this spell...");
      return;
    };

    GameObject spell = SpellSystem.instance.GetDraggingSpell ();

    if (spell != null) {
      if (charagami != null) {
        TraitSystem.instance.TryOverchargeTrait (chargingSpell);
        chargingSpell.Recharge (1);
        chargingSpell.PlayAnimation ("Refill");
        charagami.Cast (chargingSpell.gameObject);
      } else {
        Luna.instance.TalkAlways ("Are you sure about that?");
      }
    }
  }
}