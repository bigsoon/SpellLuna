using UnityEngine;
using UnityEngine.EventSystems;

public class OnDropLuna : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler {
  [SerializeField]
  private GameObject luna;

  private Color baseColor;
  private SpriteRenderer spriteRenderer;
  private bool isHovering = false;

  private void LightUpLuna () {
    spriteRenderer.material.color = Color.green;
  }

  private void LightDownLuna () {
    spriteRenderer.material.color = baseColor;
  }

  public void OnPointerEnter (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();

    isHovering = true;

    if (spell != null) {
      Protectium protectium = spell.GetComponent<Protectium> ();
      Blessy blessy = spell.GetComponent<Blessy> ();

      if (protectium != null || blessy != null) {
        LightUpLuna ();
      }
    }
  }
  public void OnPointerExit (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();

    isHovering = false;

    if (spell != null) {
      Protectium protectium = spell.GetComponent<Protectium> ();
      Blessy blessy = spell.GetComponent<Blessy> ();

      if (protectium != null || blessy != null) {
        LightDownLuna ();
      }
    }
  }

  public void OnDrop (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();
    Protectium protectium = eventData.pointerDrag.transform.parent.GetComponent<Protectium> ();
    Blessy blessy = eventData.pointerDrag.transform.parent.GetComponent<Blessy> ();

    isHovering = false;

    if (spell != null) {
      if (protectium != null) {
        protectium.Cast (luna);
        LightDownLuna ();
        return;
      }
      if (blessy != null) {
        blessy.Cast (luna);
        LightDownLuna ();
        return;
      }
    }
  }

  void Awake () {
    spriteRenderer = luna.GetComponent<SpriteRenderer> ();
    baseColor = spriteRenderer.material.color;
  }

  void Update () {
    if (!isHovering && baseColor == Color.green) {
      LightDownLuna ();
    }
  }
}