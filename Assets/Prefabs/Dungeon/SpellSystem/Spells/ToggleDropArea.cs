using UnityEngine;

public class ToggleDropArea : MonoBehaviour {
  void OnMouseDown () {
    GameObject.Find ("OnDropArea").GetComponent<CanvasGroup> ().blocksRaycasts = true;
  }

  private void TurnOffDropArea () {
    GameObject.Find ("OnDropArea").GetComponent<CanvasGroup> ().blocksRaycasts = false;
  }

  void OnMouseUp () {
    Invoke ("TurnOffDropArea", 0.1f);
  }
}