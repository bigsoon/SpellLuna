using UnityEngine;
using UnityEngine.UI;

public interface ISpell {
  void Cast (GameObject target);
}

public class Spell : MonoBehaviour, ISpell {
  [SerializeField]
  private Animator animator;
  [SerializeField]
  private GameObject spellWrapper;
  [SerializeField]
  private GameObject underneathWrapper;
  [SerializeField]
  private InfoTooltip spellInfoTooltip;
  [SerializeField]
  private LineRenderer lineRenderer;
  [SerializeField]
  private AudioSource audioSource;
  [SerializeField]
  private GameObject blockedImage;

  private int handPosition;

  private bool _canUse = false;

  public bool CanUse {
    get => _canUse;
    set => _canUse = value;
  }

  private bool _isDisabled = false;
  public bool IsDisabled {
    get => _isDisabled;
    set => _isDisabled = value;
  }
  private bool _isBlocked = false;
  public bool IsBlocked {
    get => _isBlocked;
    set => _isBlocked = value;
  }

  private void AdjustSpellInfoTooltipPosition () {
    if (handPosition == 3) {
      spellInfoTooltip.AdjustPosition (new Vector3 (420, 0));
    } else if (handPosition == 4) {
      spellInfoTooltip.AdjustPosition (new Vector3 (420, 0));
    }
  }

  public int GetHandPosition () {
    return handPosition;
  }

  public GameObject GetUnderneathWrapper () {
    return underneathWrapper;
  }

  public bool CanDrag () {
    return _canUse && !_isDisabled && !_isBlocked;
  }

  public void PlayAnimation (string clipName) {
    underneathWrapper.GetComponent<Animator> ().Play (clipName, -1, 0f);
  }

  public void SetHandPosition (int position) {
    handPosition = position;
  }

  public void MoveToPendingSpells () {
    animator.Play ("MoveToPendingSpells");
  }

  private void OnPendingSpells () {
    SpellSystem.instance.RemoveSpellFromUsed (this);
    SpellSystem.instance.AddSpellToPending (this);

    if (SpellSystem.instance.HasNoUsedSpells ()) {
      SpellSystem.instance.ShufflePendingSpells ();
      SpellSystem.instance.FillHand ();
    }
  }

  public void MoveToHand () {
    lineRenderer.enabled = true;
    SpellSystem.instance.RemoveSpellFromPending (this);
    SpellSystem.instance.AddSpellToHand (this);

    AdjustSpellInfoTooltipPosition ();

    animator.Play ("MoveToSpellSlot" + handPosition.ToString ());
    if (IsBlocked) {
      blockedImage.SetActive (true);
    }
  }

  public virtual void Cast (GameObject target) {
    audioSource.Play ();
  }

  public virtual void AutoDestruct (bool endTurn) { }

  public void OnHand () {
    _canUse = true;
    animator.enabled = false;
  }

  public void MoveToUsedSpells () {
    lineRenderer.enabled = false;
    _canUse = false;
    animator.enabled = true;

    AdjustSpellInfoTooltipPosition ();

    animator.Play ("MoveToUsedSpells" + handPosition);
  }

  private void OnUsedSpells () {
    if (IsBlocked) {
      blockedImage.SetActive (false);
      IsBlocked = false;
    }

    SpellSystem.instance.RemoveSpellFromHand (this);
    SpellSystem.instance.AddSpellToUsed (this);
  }

  void Awake () {
    lineRenderer.positionCount = 2;
    lineRenderer.startWidth = 0;
    lineRenderer.endWidth = 0.1f;
  }

  void Update () {
    lineRenderer.SetPosition (0, underneathWrapper.transform.position);
    lineRenderer.SetPosition (1, spellWrapper.transform.position);
  }
}