using System;
using UnityEngine;

[Serializable]
public class SpellsBig {
  public GameObject shottoBig;
  public GameObject protectiumBig;
  public GameObject charagamiBig;
  public GameObject electricusBig;
  public GameObject blessyBig;
  public GameObject chargottoBig;
  public GameObject shotiaBig;
  public GameObject charshoBig;
  public GameObject desintegratoBig;
}