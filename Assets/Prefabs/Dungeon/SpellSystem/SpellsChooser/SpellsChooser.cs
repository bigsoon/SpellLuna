﻿using UnityEngine;

public class SpellsChooser : MonoBehaviour {
    [SerializeField]
    private SpellsBig spellsBig;
    private GameObject[] spellsToChoose;

    void Awake () {
        spellsToChoose = new GameObject[] {
            spellsBig.blessyBig, spellsBig.chargottoBig, spellsBig.electricusBig,
            spellsBig.shotiaBig, spellsBig.charshoBig, spellsBig.desintegratoBig
        };
    }

    void Start () {
        int randomSpellIndex = Utils.GetRandomNumber (spellsToChoose.Length);

        GameObject choosedSpell = Instantiate (spellsToChoose[randomSpellIndex], new Vector3 (0, 0), Quaternion.identity);
        choosedSpell.transform.SetParent (transform, false);
    }
}