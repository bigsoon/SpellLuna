using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class DialogSystem : MonoBehaviour {
  [SerializeField]
  private Animator animator;
  [SerializeField]
  private Text displayText;
  private float typingSpeed = 0.01f;
  private bool isCoroutineRunning = false;

  IEnumerator Type (string text) {
    foreach (char letter in text.ToCharArray ()) {
      displayText.text += letter;
      yield return new WaitForSeconds (typingSpeed);
    }

    yield return new WaitForSeconds (1.5f);
    isCoroutineRunning = false;
    animator.Play ("EndDialog");
  }

  public void StartDialog (string text) {
    if (isCoroutineRunning) {
      return;
    }

    if (displayText.text.Length > 0) {
      displayText.text = null;
    };

    animator.Play ("StartDialog");
    isCoroutineRunning = true;
    StartCoroutine (Type (text));
  }

  void Update () {
    if (Luna.instance != null) {
      Luna.instance.AdjustUIElement (displayText.gameObject, 1.8f);
    }
  }
}