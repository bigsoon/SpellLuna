﻿using UnityEngine;

public class BattleStart : MonoBehaviour {
    public void OnAnimationEnd () {
        if (FightManager.instance != null) {
            FightManager.instance.TurnController.HideBattleStart ();
            FightManager.instance.StartYourTurn ();
        }
    }
}