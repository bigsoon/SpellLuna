﻿using UnityEngine;

public class FightRoomHandler : MonoBehaviour {
    public static FightRoomHandler instance;
    [SerializeField]
    private Fairy fairy;
    [SerializeField]
    private GameObject overlay;
    [SerializeField]
    private GameObject gameOverDialog;

    public void ToggleUI (bool flag) {
        foreach (Transform child in FightRoomUI.instance.transform) {
            child.gameObject.SetActive (flag);
        }
    }

    private void ToggleLoseDialog (bool flag) {
        overlay.SetActive (flag);
        gameOverDialog.SetActive (flag);
    }

    private void LoseDialogDelay () {
        ToggleLoseDialog (true);
    }

    public void TearDown () {
        FightManager.instance.CleanMonsters ();
        FightRoomHandler.instance.ToggleUI (false);
        Luna.instance.ResetShields ();
    }

    public void BackToLobby () {
        SceneLoader.instance.LoadSceneNoAnimation (1);
    }

    public void Lose () {
        TearDown ();
        FightManager.instance.gameObject.SetActive (false);
        fairy.Recall ();
        Luna.instance.Animator.enabled = true;
        Luna.instance.Animator.Play ("LunaDeath");
        DungeonBrain.instance.SaveAndTearDown ();
        Invoke ("LoseDialogDelay", 1);
    }

    public void Win () {
        DungeonBrain.instance.DamageToRemember = LifeSystem.instance.CurrentDamage;
        fairy.Deliver (new Vector3 (Luna.instance.transform.position.x, 8), () => {
            fairy.Idle ();
            DungeonBrain.instance.IncreaseFloor ();
            SceneLoader.instance.GoToNextRoom ();
        });
    }

    private void Initialize () {
        FightManager.instance.StartFight ();
        MonsterSpawner.instance.SpawnMonsters ();

        ToggleUI (true);

        SpellSystem.instance.CreatePendingSpells ();
        SpellSystem.instance.ShufflePendingSpells ();
    }

    void Start () {
        fairy.Deliver (new Vector3 (0, -2), () => {
            Luna.instance.BasePos = Luna.instance.transform.position;
            fairy.Idle ();
            Initialize ();
        });
    }

    void Awake () {
        instance = this;
    }
}