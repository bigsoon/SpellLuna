﻿using UnityEngine;

public class TurnController : MonoBehaviour {
    [SerializeField]
    private Animator battleStartAnimator;
    [SerializeField]
    private Animator yourTurnAnimator;
    [SerializeField]
    private Animator monstersTurnAnimator;

    public void HideBattleStart () {
        battleStartAnimator.gameObject.SetActive (false);
    }

    public void HideYourTurn () {
        yourTurnAnimator.gameObject.SetActive (false);
    }

    public void HideMonstersTurn () {
        monstersTurnAnimator.gameObject.SetActive (false);
    }

    public void AnimateBattleStart () {
        battleStartAnimator.gameObject.SetActive (true);
        battleStartAnimator.Play ("BattleStart");
    }

    public void AnimateYourTurn () {
        yourTurnAnimator.gameObject.SetActive (true);
        yourTurnAnimator.Play ("YourTurn");
    }
    public void AnimateMonstersTurn () {
        monstersTurnAnimator.gameObject.SetActive (true);
        monstersTurnAnimator.Play ("MonstersTurn");
    }

}