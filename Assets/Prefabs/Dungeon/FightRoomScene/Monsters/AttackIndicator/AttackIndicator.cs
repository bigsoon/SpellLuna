﻿using UnityEngine;

public class AttackIndicator : MonoBehaviour {
    [SerializeField]
    private Monster monster;
    [SerializeField]
    private GameObject normalAttack;
    [SerializeField]
    private GameObject castAttack;
    [SerializeField]
    private GameObject chargedAttack;
    [SerializeField]
    private GameObject shield;
    [SerializeField]
    private GameObject stun;
    [SerializeField]
    private LifeFace lifeFaceIndicator;
    private GameObject activeIndicator = null;

    public void Hide () {
        if (activeIndicator != null) {
            activeIndicator.SetActive (false);
            activeIndicator = null;
        }
        lifeFaceIndicator.gameObject.SetActive (false);
    }

    public void Show (GameObject attackIndicator) {
        Hide ();

        monster.AdjustUIElement (attackIndicator);
        activeIndicator = attackIndicator;
        activeIndicator.SetActive (true);
    }

    public void IndicateNormalAttack (int damage) {
        Show (normalAttack);

        IndicateDamage (damage);
    }

    public void IndicateChargedAttack (int damage) {
        Show (chargedAttack);

        IndicateDamage (damage);
    }

    public void IndicateCastAttack () {
        Show (castAttack);
    }

    public void IndicateShield () {
        Show (shield);
    }

    public void IndicateStun () {
        Show (stun);
    }

    public void IndicateDamage (float damage) {
        float skullsCount = Mathf.Ceil (damage / Constants.LIFE_FACE_MAX_PARTS);

        for (int i = 0; i < skullsCount; i++) {
            float desiredXPos = Utils.CalculateDesiredXPos (0.5f, skullsCount, i);
            Vector3 desiredPos = transform.position + new Vector3 (desiredXPos, 0.5f);

            lifeFaceIndicator.gameObject.SetActive (true);
            lifeFaceIndicator.ActivateSkulls (damage);

            monster.AdjustUIElement (lifeFaceIndicator.gameObject, 2);

            if (damage > Constants.LIFE_FACE_MAX_PARTS) {
                damage -= Constants.LIFE_FACE_MAX_PARTS;
            }
        }

    }
}