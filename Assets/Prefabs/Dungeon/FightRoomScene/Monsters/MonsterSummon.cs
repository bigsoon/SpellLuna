using UnityEngine;

public class MonsterSummon : MonoBehaviour {
  public GameObject summonEffectPrefab;

  public GameObject orbForm;
  public GameObject monsterForm;

  private void CreateSummonEffect () {
    orbForm.SetActive (false);
    Instantiate (summonEffectPrefab, transform.position, Quaternion.identity);
  }

  private void TransformIntoMonsterForm () {
    monsterForm.SetActive (true);

    FightManager.instance.AddMonster (monsterForm.GetComponent<Monster> ());
  }

  void Start () {
    Invoke ("CreateSummonEffect", 0.7f);
    Invoke ("TransformIntoMonsterForm", 0.85f);
  }

}