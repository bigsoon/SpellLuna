﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    [SerializeField]
    private Monster monster;
    [SerializeField]
    private Slider healthBarSlider;
    [SerializeField]
    private Text healthText;

    public void RedrawHealth (int health, int maxHealth) {
        healthBarSlider.value = health;
        healthText.text = health.ToString () + "/" + maxHealth.ToString ();
    }

    void Start () {
        int health = monster.Health;
        int maxHealth = monster.MaxHealth;

        healthBarSlider.maxValue = maxHealth;

        RedrawHealth (health, maxHealth);
    }

    void Update () {
        monster.AdjustUIElement (healthBarSlider.gameObject, 1);
    }
}