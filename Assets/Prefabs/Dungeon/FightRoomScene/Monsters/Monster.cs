using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {
  [SerializeField]
  private GameObject dieEffectPrefab;
  [SerializeField]
  private GameObject _hitEffectPrefab;
  public GameObject HitEffectPrefab {
    get => _hitEffectPrefab;
  }

  [SerializeField]
  private GameObject bloodEffectPrefab;

  [SerializeField]
  private HealthBar healthBar;
  [SerializeField]
  private AttackIndicator _attackIndicator;
  public AttackIndicator AttackIndicator {
    get => _attackIndicator;
  }

  [SerializeField]
  private Animator animator;
  [SerializeField]
  private AudioSource audioSource;
  [SerializeField]
  private int _health;
  public int Health {
    get => _health;
  }
  private int _maxHealth;
  public int MaxHealth {
    get => _maxHealth;
  }

  [SerializeField]
  private int _attackDamage;
  public int AttackDamage {
    get => _attackDamage;
  }

  [SerializeField]
  private int worth = 10;

  private int baseAttackDamage;

  private List<Waypoint> waypoints;

  private Vector3 basePos;

  private int shields = 0;
  private bool isMoving;
  private bool _isStunned = false;
  public bool IsStunned {
    get => _isStunned;
    set => _isStunned = value;
  }
  private bool _isHovered = false;
  public bool IsHovered {
    get => _isHovered;
    set => _isHovered = value;
  }

  private void NextMonsterAttack () {
    FightManager.instance.ExecuteNextMonsterAttack ();
  }

  private void Die () {
    int randomLootNumber = Random.Range (1, worth / 2);

    for (int i = 0; i < randomLootNumber; i++) {
      LootSystem.instance.SpawnLoot (transform.position, worth);
    }

    Instantiate (dieEffectPrefab, transform.position, transform.rotation);
    FightManager.instance.KillMonster (this);
    Destroy (this.gameObject);
  }

  private void Move () {
    Waypoint currentWaypoint = waypoints[0];
    float waypointSpeed = currentWaypoint.Speed;
    Vector3 waypointPos = currentWaypoint.Pos;
    bool reachedWaypoint = Vector3.Distance (transform.position, waypointPos) < 0.1f;

    if (reachedWaypoint) {
      waypoints.Remove (currentWaypoint);

      if (waypoints.Count == 0) {
        isMoving = false;
        Invoke ("NextMonsterAttack", 0.5f);
        return;
      }

      currentWaypoint = waypoints[0];
    }
    transform.position = Utils.MoveTowardsTarget (transform.position, waypointPos, waypointSpeed);
  }

  public void AdjustUIElement (GameObject objectToAdjust, float offset = 1.5f) {
    Vector3 activeIndicatorPos = Camera.main.WorldToScreenPoint (transform.position + new Vector3 (0, offset));
    objectToAdjust.transform.position = activeIndicatorPos;
  }

  public void HandleAttackAnimationEnd () { }

  public void Stun () {
    _attackIndicator.Hide ();
    _attackIndicator.IndicateStun ();

    _isStunned = true;
  }

  public void TakeDamage (int damage) {
    Instantiate (bloodEffectPrefab, transform.position, transform.rotation);
    animator.SetTrigger ("onHit");
    audioSource.PlayOneShot (audioSource.clip);

    _health -= damage;

    if (_health <= 0) {
      Die ();
    }

    healthBar.RedrawHealth (_health, _maxHealth);
  }

  public virtual void ChooseNextAttack () { }

  public virtual void Attack () {
    _attackIndicator.Hide ();

    if (_isStunned) {
      return;
    }

    waypoints.Add (new Waypoint (10, basePos + new Vector3 (0, -0.5f)));
    waypoints.Add (new Waypoint (10, basePos));
    isMoving = true;
  }

  void Awake () {
    basePos = transform.position;
    waypoints = new List<Waypoint> ();
  }

  void Start () {
    baseAttackDamage = _attackDamage;
    _maxHealth = _health;
  }

  void Update () {
    if (isMoving) {
      Move ();
    }
  }
}