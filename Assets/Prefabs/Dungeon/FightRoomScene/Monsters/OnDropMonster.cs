using UnityEngine;
using UnityEngine.EventSystems;

public class OnDropMonster : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler {
  [SerializeField]
  private Monster monster;
  [SerializeField]
  private SpriteRenderer spriteRenderer;
  private Color baseColor;

  private void LightUpMonster () {
    spriteRenderer.material.color = Color.yellow;
  }

  private void LightDownMonster () {
    spriteRenderer.material.color = baseColor;
  }

  public void OnPointerEnter (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();
    monster.IsHovered = true;

    if (spell != null) {
      Shotto shotto = spell.GetComponent<Shotto> ();
      Transformato transformato = spell.GetComponent<Transformato> ();
      Desintegrato desintegrato = spell.GetComponent<Desintegrato> ();

      if (shotto != null || transformato != null || desintegrato != null) {
        LightUpMonster ();
      }
    }
  }
  public void OnPointerExit (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();
    monster.IsHovered = false;

    if (spell != null) {
      Shotto shotto = spell.GetComponent<Shotto> ();
      Transformato transformato = spell.GetComponent<Transformato> ();
      Desintegrato desintegrato = spell.GetComponent<Desintegrato> ();

      if (shotto != null || transformato != null || desintegrato != null) {
        LightDownMonster ();
      }
    }
  }

  public void OnDrop (PointerEventData eventData) {
    GameObject spell = SpellSystem.instance.GetDraggingSpell ();
    Shotto shotto = eventData.pointerDrag.transform.parent.GetComponent<Shotto> ();
    Transformato transformato = eventData.pointerDrag.transform.parent.GetComponent<Transformato> ();
    Desintegrato desintegrato = spell.GetComponent<Desintegrato> ();

    monster.IsHovered = false;

    if (spell != null) {
      if (shotto != null) {
        LightDownMonster ();
        shotto.Cast (monster.gameObject);
        return;
      }
      if (transformato != null) {
        LightDownMonster ();
        transformato.Cast (monster.gameObject);
        return;
      }

      if (desintegrato != null) {
        LightDownMonster ();
        desintegrato.Cast (monster.gameObject);
        return;
      }

      Luna.instance.TalkAlways ("Didn't you want to use a different spell there?");
    }
  }

  void Awake () {
    baseColor = spriteRenderer.material.color;
  }

  void Update () {
    monster.AdjustUIElement (gameObject, 0.5f);

    if (!monster.IsHovered) {
      LightDownMonster ();
    }
  }
}