using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterBullet : MonoBehaviour {
  private int bulletDamage;

  private GameObject target;

  private float speed;

  public void SetBulletDamage (int damage) {
    bulletDamage = damage;
  }

  // void OnCollisionEnter2D (Collision2D collision) {
  //   GameObject collider = collision.gameObject;

  //   if (collider == target) {
  //     Luna luna = collider.GetComponent<Luna> ();

  //     luna.Hit (bulletDamage);
  //     // TODO get back to it!
  //     // transform.parent.GetComponent<Monster> ().ClearAttackIndicator ();
  //     FightManager.instance.ExecuteNextMonsterAttack ();

  //     Destroy (this.gameObject);
  //   }
  // }

  void Awake () {
    speed = 10;
  }

  void Start () {
    target = GameObject.Find ("Luna");
    transform.rotation = Utils.RotateTowardsTarget (transform, target.transform);
  }

  void Update () {
    if (target) {
      transform.position = Utils.MoveTowardsTarget (transform.position, target.transform.position, speed);
    }
  }
}