using System.Collections.Generic;
using UnityEngine;

public class Spider : Monster {
  [SerializeField]
  private GameObject blockSpellEffectPrefab;
  private int random;
  private void NormalAttack () {
    Instantiate (HitEffectPrefab, Luna.instance.transform.position, Quaternion.identity);
    Luna.instance.Hit (AttackDamage);
  }

  private void BlockSpellSlot () {
    int randomFreeSpellSlotIndex = 0;
    bool randomFreeSpellSlot = true;

    while (randomFreeSpellSlot) {
      randomFreeSpellSlotIndex = Random.Range (0, 5);
      randomFreeSpellSlot = SpellSystem.instance.BlockedSpellSlots[randomFreeSpellSlotIndex];
    }
    Instantiate (blockSpellEffectPrefab, Luna.instance.transform.position, Quaternion.identity);
    SpellSystem.instance.BlockSpellSlot (randomFreeSpellSlotIndex);
  }

  private void CastSpell () {
    Spell spellToAdd = SpellSystem.instance.CreateNewSpell (DeckGenerator.instance.Spells.transformato);

    Instantiate (blockSpellEffectPrefab, Luna.instance.transform.position, Quaternion.identity);
    SpellSystem.instance.AddSpellToPending (spellToAdd);
  }

  public void NextMonsterAttack () {
    FightManager.instance.ExecuteNextMonsterAttack ();
  }

  public override void ChooseNextAttack () {
    random = Random.Range (1, 101);

    if (random <= 50) {
      AttackIndicator.IndicateNormalAttack (AttackDamage);
    } else if (random > 50 && random <= 70) {
      AttackIndicator.IndicateCastAttack ();
    } else if (random > 70 && random <= 100) {
      AttackIndicator.IndicateCastAttack ();
    }
  }

  public override void Attack () {
    base.Attack ();

    if (IsStunned) {
      Invoke ("NextMonsterAttack", 0.5f);
      IsStunned = false;

      return;
    }

    if (random <= 50) {
      NormalAttack ();
    } else if (random <= 70) {
      BlockSpellSlot ();
    } else if (random <= 100) {
      CastSpell ();
    }
  }
}