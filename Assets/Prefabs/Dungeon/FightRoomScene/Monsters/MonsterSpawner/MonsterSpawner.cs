﻿using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour {
    public static MonsterSpawner instance;
    public Monsters monsters;
    [SerializeField]
    private GameObject spawnPointOne;
    [SerializeField]
    private GameObject spawnPointTwo;
    [SerializeField]
    private GameObject spawnPointThree;
    [SerializeField]
    private GameObject spawnPointFour;
    private List<GameObject> spawnPoints = null;

    public void SpawnMonsterAlways (GameObject spawnPoint) {
        spawnPoint.SetActive (true);
        GameObject monster = Instantiate (monsters.spider, spawnPoint.transform.position + new Vector3 (0, 0.5f), Quaternion.identity);

        spawnPoints.Remove (spawnPoint);
    }

    public void SpawnMonster (GameObject spawnPoint) {
        float randomNumber = Random.Range (10 + DungeonBrain.instance.DangerLevel / 4, 100);

        if (randomNumber >= 50) {
            // TODO choose monster based on danger level
            spawnPoint.SetActive (true);
            GameObject monster = Instantiate (monsters.spider, spawnPoint.transform.position + new Vector3 (0, 0.5f), Quaternion.identity);
        }
    }

    public void SpawnMonsters () {
        int firstSpawnPointIndex = Utils.GetRandomNumber (spawnPoints.Count);

        SpawnMonsterAlways (spawnPoints[firstSpawnPointIndex]);

        foreach (GameObject spawnPoint in spawnPoints) {
            SpawnMonster (spawnPoint);
        }
        spawnPoints.Clear ();
    }

    void Awake () {
        spawnPoints = new List<GameObject> ();

        spawnPoints.Add (spawnPointOne);
        spawnPoints.Add (spawnPointTwo);
        spawnPoints.Add (spawnPointThree);
        spawnPoints.Add (spawnPointFour);

        instance = this;
    }
}