﻿using UnityEngine;

public class YourTurn : MonoBehaviour {
    public void OnAnimationEnd () {
        FightManager.instance.TurnController.HideYourTurn ();
        FightManager.instance.HandleYourTurn ();
    }
}