﻿using UnityEngine;

public class MonstersTurn : MonoBehaviour {
    public void OnAnimationEnd () {
        FightManager.instance.TurnController.HideMonstersTurn ();
        FightManager.instance.HandleMonstersTurn ();
    }
}