using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Tooltip : MonoBehaviour {
  [SerializeField]
  private RectTransform tooltipBackground;
  [SerializeField]
  private Text tooltipText;

  public void ShowTooltipText (string text) {
    tooltipText.text = text;

    Vector3 backgroundSize = new Vector3 (tooltipText.preferredWidth + 10f, tooltipText.preferredHeight + 10f);
    tooltipBackground.sizeDelta = backgroundSize;
  }

  void Start () {
    // TODO buying new spells out of dungeon
    // TODO buying spell slots out of dungeon
  }
}