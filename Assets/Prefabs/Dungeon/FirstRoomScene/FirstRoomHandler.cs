﻿using UnityEngine;

public class FirstRoomHandler : MonoBehaviour {
    public static FirstRoomHandler instance;

    public LunaSummon lunaSummon;

    public Fairy fairy;

    public GameObject dialogPrefab;

    public GameObject overlayPrefab;

    private void SummonStartingFairy () {
        fairy.gameObject.SetActive (true);
        fairy.Summon (() => fairy.Transform (() => ActivateDialog ()));
    }

    private void ActivateDialog () {
        overlayPrefab.SetActive (true);
        dialogPrefab.SetActive (true);
    }

    public void DeactivateDialog () {
        overlayPrefab.SetActive (false);
        dialogPrefab.SetActive (false);

        fairy.Deliver (new Vector3 (0, 8), () => {
            fairy.Idle ();
            SceneLoader.instance.GoToNextRoom ();
        });
    }

    void Awake () {
        instance = this;
    }

    void Start () {
        Pause.instance.Initialize ();
        Gold.instance.Initialize ();
        lunaSummon.Summon ();
        Invoke ("SummonStartingFairy", 2);
    }
}