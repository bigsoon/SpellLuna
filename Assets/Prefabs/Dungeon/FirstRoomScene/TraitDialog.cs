﻿using UnityEngine;

public class TraitDialog : MonoBehaviour {
    public void AddStartingOffensiveTrait () {
        TraitSystem.instance.AddNewTrait (TraitSystem.instance.Traits.lifeSteal);
        DeckGenerator.instance.GenerateOffensiveDeck ();
        FirstRoomHandler.instance.DeactivateDialog ();
    }
    public void AddStartingDefensiveTrait () {
        TraitSystem.instance.AddNewTrait (TraitSystem.instance.Traits.protector);
        DeckGenerator.instance.GenerateDefensiveDeck ();
        FirstRoomHandler.instance.DeactivateDialog ();
    }
    public void AddStartingUtilityTrait () {
        TraitSystem.instance.AddNewTrait (TraitSystem.instance.Traits.overcharge);
        DeckGenerator.instance.GenerateUtilityDeck ();
        FirstRoomHandler.instance.DeactivateDialog ();
    }
}