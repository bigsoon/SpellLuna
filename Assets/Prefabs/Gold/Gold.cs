﻿using UnityEngine;
using UnityEngine.UI;

public class Gold : MonoBehaviour {
    public static Gold instance;
    [SerializeField]
    private GameObject goldWrapper;
    [SerializeField]
    private Text goldText;
    [SerializeField]
    private Animator animator;

    private void ToggleGoldWrapper (bool flag) => goldWrapper.SetActive (flag);

    private string GetGoldValue () => SceneLoader.instance.GetSceneName () == "Lobby" ?
        PlayerData.instance.Currency.ToString () : DungeonBrain.instance.GoldGathered.ToString ();

    public void Increase () {
        animator.Play ("Increase", -1, 0f);
    }

    public void Initialize () {
        ToggleGoldWrapper (true);
    }

    public void TearDown () {
        ToggleGoldWrapper (false);
    }

    void Awake () {
        instance = this;
    }

    void Start () {
        goldText.text = GetGoldValue ();
    }

    void Update () {
        goldText.text = GetGoldValue ();
    }
}