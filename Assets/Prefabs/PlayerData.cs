﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {
    public static PlayerData instance;

    private List<int> _unlockedSpells;

    private List<int> _availableRunes;

    private List<int> _unlockedEasterEggs;

    private int _currency = 0;

    public int Currency {
        get => _currency;
        set => _currency = value;
    }

    private int _premiumCurrency = 0;

    public int PremiumCurrency {
        get => _premiumCurrency;
        set => _premiumCurrency = value;
    }

    private int _energy = 20;

    public int Energy {
        get => _energy;
        set => _energy = value;
    }

    private int _floorReached = 1;

    public int FlooReached {
        get => _floorReached;
        set => _floorReached = value;
    }

    private void LoadPlayerData () {
        int floorReachedPrefs = PlayerPrefs.GetInt ("FloorReached");

        if (floorReachedPrefs == 0) {
            return;
        }

        _currency = PlayerPrefs.GetInt ("Currency");
        _premiumCurrency = PlayerPrefs.GetInt ("PremiumCurrency");
        _energy = PlayerPrefs.GetInt ("Energy");
        _floorReached = floorReachedPrefs;
    }

    private void ResetPlayerData () {
        PlayerPrefs.SetInt ("Currency", 0);
        PlayerPrefs.SetInt ("PremiumCurrency", 0);
        PlayerPrefs.SetInt ("Energy", 20);
        PlayerPrefs.SetInt ("FloorReached", 0);
    }

    private void ResetEnergy () {
        PlayerPrefs.SetInt ("Energy", 20);
    }

    public void SavePlayerData () {
        PlayerPrefs.SetInt ("Currency", _currency);
        PlayerPrefs.SetInt ("PremiumCurrency", _premiumCurrency);
        PlayerPrefs.SetInt ("Energy", _energy);
        PlayerPrefs.SetInt ("FloorReached", _floorReached);
    }

    public void AddCurrency () {
        _currency++;
        SavePlayerData ();
    }

    public void AddCurrency (int currency) {
        _currency += currency;;
        SavePlayerData ();
    }

    public void SaveBestFloor (int floor) {
        if (floor > _floorReached) {
            _floorReached = floor;
            SavePlayerData ();
        }
    }

    public void UseEnergy () {
        _energy -= 5;
        SavePlayerData ();
    }

    public bool OutOfEnergy () => _energy < 5;

    void Awake () {
        instance = this;
        // TODO remove energy later?
        ResetEnergy ();

        LoadPlayerData ();

        InvokeRepeating ("SavePlayerData", 0, 5);
    }
}