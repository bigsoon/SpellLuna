﻿using UnityEngine;

public class ResizeToScreen : MonoBehaviour {
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    private void ResizeSpriteToScreen () {
        transform.localScale = new Vector3 (1, 1);

        float width = spriteRenderer.sprite.bounds.size.x;
        float height = spriteRenderer.sprite.bounds.size.y;

        double worldScreenHeight = Camera.main.orthographicSize * 2.0;
        double worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        transform.localScale = new Vector3 ((float) worldScreenWidth / width, (float) worldScreenHeight / height);
    }
    void Awake () {
        ResizeSpriteToScreen ();
    }

}