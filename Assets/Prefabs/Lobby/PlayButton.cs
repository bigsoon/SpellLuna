﻿using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour {
    [SerializeField]
    private GameObject energyIcon;
    [SerializeField]
    private Button button;
    [SerializeField]
    private Animator animator;

    void OnClick () {
        energyIcon.GetComponent<Animator> ().Play ("Decrease");
        PlayerData.instance.UseEnergy ();
        DungeonGenerator.instance.GenerateDungeon ();
        SceneLoader.instance.LoadNextScene ();
    }

    void Awake () {
        // TODO Propose energy charging for $$$ / ADs
        if (PlayerData.instance.OutOfEnergy ()) {
            animator.Play ("DisableButton");
            button.interactable = false;
        }

        Pause.instance.Resume ();
    }

    void Start () {
        button.onClick.AddListener (OnClick);
    }

}