﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsButton : MonoBehaviour {
    private Button button;

    void OnClick () {
        SceneLoader.instance.LoadScene (4);
    }

    void Awake () {
        button = GetComponent<Button> ();
    }

    void Start () {
        button.onClick.AddListener (OnClick);
    }
}