﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyGUI : MonoBehaviour {
    public Text topFloorText;
    public Text energyText;
    public Text currencyText;
    public Text premiumCurrencyText;

    public GameObject energyFillInnerWrapper;

    private void UpdateTexts () {
        topFloorText.text = "Top Floor: " + PlayerData.instance.FlooReached.ToString ();
        energyText.text = PlayerData.instance.Energy.ToString () + "/" + "20";
        currencyText.text = PlayerData.instance.Currency.ToString ();
        premiumCurrencyText.text = PlayerData.instance.PremiumCurrency.ToString ();

        float energyScale = PlayerData.instance.Energy / 20.0f > 1 ? 1 : PlayerData.instance.Energy / 20.0f;

        energyFillInnerWrapper.transform.localScale = new Vector3 (energyScale, 1, 1);
    }

    void Start () {
        UpdateTexts ();
    }

    // Update is called once per frame
    void Update () {
        UpdateTexts ();
    }

}