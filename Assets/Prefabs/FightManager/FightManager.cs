﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightManager : MonoBehaviour {
    public static FightManager instance;
    [SerializeField]
    private TurnController _turnController;
    public TurnController TurnController {
        get => _turnController;
    }

    [SerializeField]
    private Button endTurnButton;
    private List<Monster> _monsters;
    public List<Monster> Monsters {
        get => _monsters;
    }
    private int monsterAttacksCounter;

    public void DamageAllMonsters (int damage) {
        for (int i = 0; i < _monsters.Count; i++) {
            _monsters[i].TakeDamage (damage);
        }
    }
    public Monster GetRandomMonster () => _monsters[Random.Range (0, _monsters.Count)];
    private void EnableTurnButton () => endTurnButton.interactable = true;
    private void DisableTurnButton () => endTurnButton.interactable = false;

    private void ChooseNextMonstersAttack () {
        for (int i = 0; i < _monsters.Count; i++) {
            _monsters[i].ChooseNextAttack ();
        }
    }
    public void ExecuteNextMonsterAttack () {
        if (AllMonstersExecutedAttack ()) {
            ResetMonsterAttacksCounter ();
            StartYourTurn ();
            return;
        }
        IncrementMonsterAttacksCounter ();

        _monsters[monsterAttacksCounter - 1].Attack ();
    }

    public void IncrementMonsterAttacksCounter () => monsterAttacksCounter++;

    public void ResetMonsterAttacksCounter () => monsterAttacksCounter = 0;

    public bool AllMonstersExecutedAttack () => _monsters.Count == monsterAttacksCounter;

    public bool HasAliveMonsters () => _monsters.Count > 0;

    public void AddMonster (Monster monster) => _monsters.Add (monster);

    private void TearDownSpellSystem () {
        SpellSystem.instance.CleanHand ();
        SpellSystem.instance.ResetSpellSlots ();
    }

    private void OnWin () {
        FightRoomHandler.instance.Win ();
    }

    public void CleanMonsters () {
        Monsters.Clear ();
    }

    public void KillMonster (Monster monster) {
        TraitSystem.instance.TryLifeStealTrait ();
        _monsters.Remove (monster);

        if (!HasAliveMonsters ()) {
            FightRoomHandler.instance.TearDown ();
            Luna.instance.TalkFromAlways (new [] {
                "Nice fight!",
                "I wonder where I left my donuts...",
                "Stylish!",
                "I wonder if this fairy ever gets tired...?"
            });
            Invoke ("OnWin", 2);
        }
    }

    public void StartFight () {
        _turnController.AnimateBattleStart ();
    }

    public void StartYourTurn () {
        Luna.instance.ResetShields ();
        _turnController.AnimateYourTurn ();
    }

    public void EndYourTurn () {
        DisableTurnButton ();
        TearDownSpellSystem ();

        TooltipManager.instance.HideActiveTooltip ();
        _turnController.AnimateMonstersTurn ();
    }

    public void HandleYourTurn () {
        SpellSystem.instance.FillHand ();
        ChooseNextMonstersAttack ();
        Invoke ("EnableTurnButton", 1);
    }

    public void HandleMonstersTurn () {
        ExecuteNextMonsterAttack ();
    }

    void Awake () {
        _monsters = new List<Monster> ();
        instance = this;
    }
}