﻿using System;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour {
    public static DungeonGenerator instance;
    private static int FIRST_ROOM_INDEX = 2;
    private static int FIGHT_ROOM_INDEX = 3;
    private static int MONSTERS_WAVES_ROOM_INDEX = 4;
    private static int BOSS_ROOM_INDEX = 5;
    private static int ARCADE_ROOM_INDEX = 6;
    private static int FAIRY_CHALLENGE_ROOM_INDEX = 7;
    private static int EVENT_ROOM_INDEX = 8;
    private static int SHOP_ROOM_INDEX = 9;
    private static int FOUNTAIN_ROOM_INDEX = 10;
    private static int TRAIT_ROOM_INDEX = 11;

    private int[] _rooms;
    public int[] Rooms {
        get => _rooms;
    }
    private int[] _standardRooms;
    private int[] _fightRooms;
    private int[] _nonFightRooms;
    private int[] _specialRooms;

    private void FillStartingRooms () {
        _rooms[0] = FIRST_ROOM_INDEX;
        _rooms[1] = FIGHT_ROOM_INDEX;
    }

    private void FillStandardRooms () {
        int fightRoomsInARow = 1;
        int nonFightRoomsInARow = 0;

        for (int i = 2; i < _rooms.Length; i++) {
            if (_rooms[i] != 0) {
                continue;
            }

            int standardRoomIndex = _standardRooms[Utils.GetRandomNumber (_standardRooms.Length)];

            // Can't have more than 3 fight rooms in a row
            if (fightRoomsInARow == 3) {
                int nonFightRoomIndex = _nonFightRooms[Utils.GetRandomNumber (_nonFightRooms.Length)];

                nonFightRoomsInARow++;
                fightRoomsInARow = 0;
                _rooms[i] = nonFightRoomIndex;
                continue;
            }

            // Can't have more than 1 non-fight rooms in a row
            if (nonFightRoomsInARow == 1) {
                int fightRoomIndex = _fightRooms[Utils.GetRandomNumber (_fightRooms.Length)];
                fightRoomsInARow++;
                nonFightRoomsInARow = 0;
                _rooms[i] = fightRoomIndex;
                continue;
            }

            if (Array.IndexOf (_fightRooms, standardRoomIndex) > -1) {
                fightRoomsInARow++;
                nonFightRoomsInARow = 0;
            } else if (Array.IndexOf (_nonFightRooms, standardRoomIndex) > -1) {
                nonFightRoomsInARow++;
                fightRoomsInARow = 0;
            }

            _rooms[i] = standardRoomIndex;
        }
    }

    private void FillSpecialRooms () {
        for (int i = 7; i < _rooms.Length; i += 7) {
            int specialRoomIndex = _specialRooms[Utils.GetRandomNumber (_specialRooms.Length)];

            _rooms[i] = specialRoomIndex;
        }
    }

    private void FillReservedRooms () {
        // Traits, one starting + 9 to gain through the game
        _rooms[10] = TRAIT_ROOM_INDEX;
        _rooms[20] = TRAIT_ROOM_INDEX;
        _rooms[30] = TRAIT_ROOM_INDEX;
        _rooms[40] = TRAIT_ROOM_INDEX;
        _rooms[50] = TRAIT_ROOM_INDEX;
        _rooms[60] = TRAIT_ROOM_INDEX;
        _rooms[70] = TRAIT_ROOM_INDEX;
        _rooms[80] = TRAIT_ROOM_INDEX;
        _rooms[90] = TRAIT_ROOM_INDEX;
        // Bosses, 4 every 25 levels
        _rooms[25] = BOSS_ROOM_INDEX;
        _rooms[50] = BOSS_ROOM_INDEX;
        _rooms[75] = BOSS_ROOM_INDEX;
        _rooms[100] = BOSS_ROOM_INDEX;
    }

    public void GenerateDungeon () {
        FillStartingRooms ();
        FillSpecialRooms ();
        FillReservedRooms ();
        FillStandardRooms ();
    }

    public void FillFallWithMonsters (int currentFloor) {
        _rooms[currentFloor] = FIGHT_ROOM_INDEX;
    }

    void Awake () {
        _rooms = new int[101];
        _standardRooms = new [] {
            FIGHT_ROOM_INDEX,
            MONSTERS_WAVES_ROOM_INDEX,
            ARCADE_ROOM_INDEX,
            FAIRY_CHALLENGE_ROOM_INDEX,
            EVENT_ROOM_INDEX
        };
        _fightRooms = new [] {
            FIGHT_ROOM_INDEX,
            MONSTERS_WAVES_ROOM_INDEX,
        };
        _nonFightRooms = new [] {
            ARCADE_ROOM_INDEX,
            FAIRY_CHALLENGE_ROOM_INDEX,
            EVENT_ROOM_INDEX
        };
        _specialRooms = new [] { SHOP_ROOM_INDEX, FOUNTAIN_ROOM_INDEX };

        instance = this;
    }

    void Start () {
        GenerateDungeon ();
    }
}