﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckGenerator : MonoBehaviour {
    public static DeckGenerator instance;

    [SerializeField]
    private Spells _spells;
    public Spells Spells {
        get => _spells;
    }
    private Spell[] _offensiveSpells;
    public Spell[] OffensiveSpells {
        get => _offensiveSpells;
    }
    private Spell[] _defensiveSpells;
    public Spell[] DefensiveSpells {
        get => _defensiveSpells;
    }
    private Spell[] _utilitySpells;
    public Spell[] UtilitySpells {
        get => _utilitySpells;
    }
    private List<Spell> _deck;

    public List<Spell> Deck {
        get => _deck;
    }
    public void GenerateOffensiveDeck () {
        _deck.Clear ();

        for (int i = 0; i < 15; i++) {
            Spell offensiveSpell = _offensiveSpells[Utils.GetRandomNumber (_offensiveSpells.Length)];
            _deck.Add (offensiveSpell);
        }
        for (int i = 0; i < 5; i++) {
            Spell defensiveSpell = _defensiveSpells[Utils.GetRandomNumber (_defensiveSpells.Length)];
            _deck.Add (defensiveSpell);
        }
        for (int i = 0; i < 10; i++) {
            Spell utilitySpell = _utilitySpells[Utils.GetRandomNumber (_utilitySpells.Length)];
            _deck.Add (utilitySpell);
        }
    }

    public void GenerateDefensiveDeck () {
        _deck.Clear ();

        for (int i = 0; i < 5; i++) {
            Spell offensiveSpell = _offensiveSpells[Utils.GetRandomNumber (_offensiveSpells.Length)];
            _deck.Add (offensiveSpell);
        }
        for (int i = 0; i < 15; i++) {
            Spell defensiveSpell = _defensiveSpells[Utils.GetRandomNumber (_defensiveSpells.Length)];
            _deck.Add (defensiveSpell);
        }
        for (int i = 0; i < 10; i++) {
            Spell utilitySpell = _utilitySpells[Utils.GetRandomNumber (_utilitySpells.Length)];
            _deck.Add (utilitySpell);
        }
    }

    public void GenerateUtilityDeck () {
        _deck.Clear ();

        for (int i = 0; i < 10; i++) {
            Spell offensiveSpell = _offensiveSpells[Utils.GetRandomNumber (_offensiveSpells.Length)];
            _deck.Add (offensiveSpell);
        }
        for (int i = 0; i < 10; i++) {
            Spell defensiveSpell = _defensiveSpells[Utils.GetRandomNumber (_defensiveSpells.Length)];
            _deck.Add (defensiveSpell);
        }
        for (int i = 0; i < 10; i++) {
            Spell utilitySpell = _utilitySpells[Utils.GetRandomNumber (_utilitySpells.Length)];
            _deck.Add (utilitySpell);
        }
    }

    void Awake () {
        _deck = new List<Spell> ();
        // _offensiveSpells = new Spell[] {
        //     _spells.shotto, _spells.electricus, _spells.shotia, _spells.charsho, _spells.desintegrato
        // };
        // _defensiveSpells = new Spell[] {
        //     _spells.blessy, _spells.protectium
        // };
        // _utilitySpells = new Spell[] {
        //     _spells.charagami, _spells.chargotto
        // };
        _offensiveSpells = new Spell[] {
            _spells.shotto, _spells.shotia
        };
        _defensiveSpells = new Spell[] {
            _spells.protectium
        };
        _utilitySpells = new Spell[] {
            _spells.charagami
        };
        GenerateOffensiveDeck ();
        instance = this;
    }
}