﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transitioner : MonoBehaviour {
    public static Transitioner instance;

    public delegate void Callback ();

    enum State {
        Enlarge,
        Shrink,
        Idle
    }

    private bool isTransitioning = false;

    private State currentState = State.Idle;

    private GameObject go = null;

    private Vector3 desiredScale;

    private Callback callback;

    public void SetTarget (GameObject target) {
        go = target;
    }

    public void Enlarge (Vector3 scale, Callback callback) {
        go.transform.localScale = new Vector3 (0, 0.5f);
        currentState = State.Enlarge;
        desiredScale = scale;

        this.callback = callback;
    }

    public void Shrink () {
        Invoke ("DoShrink", 0.3f);
    }

    private void DoShrink () {
        currentState = State.Shrink;
    }

    private void Idle () {
        currentState = State.Idle;
    }

    private void Clean () {
        Destroy (go);

        go = null;
        callback = null;
    }

    private void HandleEnlarging () {
        go.transform.localScale += new Vector3 (1 * Time.deltaTime, 0, 0);

        if (go.transform.localScale.x >= desiredScale.x) {
            go.transform.localScale = desiredScale;

            Idle ();
            callback ();
        }
    }

    private void HandleShrinking () {
        go.transform.localScale -= new Vector3 (1 * Time.deltaTime, 0, 0);

        if (go.transform.localScale.x <= 0) {
            go.transform.localScale = new Vector3 (0, 0);

            Idle ();
            Clean ();
        }
    }

    void Awake () {
        if (instance == null) {
            DontDestroyOnLoad (gameObject);
            instance = this;
        } else if (instance != this) {
            Destroy (gameObject);
        }
    }

    void Update () {
        switch (currentState) {
            case State.Idle:
                break;
            case State.Enlarge:
                HandleEnlarging ();
                break;
            case State.Shrink:
                HandleShrinking ();
                break;
            default:
                break;
        }
    }
}